package GerarProva;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import javax.swing.JOptionPane;

public class Gabarito {
    
    public void gerar(String saida,String nome) {
        
        Document documentoPDF = new Document(PageSize.A4);
        
        try{
            PdfWriter.getInstance(documentoPDF, new FileOutputStream(System.getProperty("user.dir")+"/src/resources/gabarito.pdf"));
            documentoPDF.open();
            //documentoPDF.setPageSize(PageSize.A4);
            
            Font fonteTexto = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            
            Image cima = Image.getInstance(System.getProperty("user.dir")+"/src/resources/cimaGabarito.jpg");
            cima.scalePercent(25, 22);
            cima.setAlignment(Element.ALIGN_CENTER);
            documentoPDF.add(cima);
            
            Paragraph nomeDisciplina = new Paragraph("NOME DO(A) ACADÊMICO(A): "+nome,fonteTexto);
            nomeDisciplina.setAlignment(Element.ALIGN_CENTER);
            documentoPDF.add(nomeDisciplina);
            
            Image baixo = Image.getInstance(System.getProperty("user.dir")+"/src/resources/baixoGabarito.jpg");
            baixo.scalePercent(25, 22);
            baixo.setAlignment(Element.ALIGN_CENTER);
            documentoPDF.add(baixo);
                                    
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }finally{
            documentoPDF.close();
        }
   
    }
    
}
