package GerarProva;

import Principal.Capa;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;


public class GerarProva {
    
    private final String url = "jdbc:mysql://"+new Ip.Servidor().getIp()+":3306/edef";
    private final String username = new Ip.Servidor().getUsuario();
    private final String password = new Ip.Servidor().getSenha();
    
    private ArrayList<String> provas = new ArrayList<>();
    private ArrayList<String> provaFinal = new ArrayList<>();
    private ArrayList<String> txts = new ArrayList<>();
        
    private int numeracao = 11;
    int provas_segundo_dia = 1;
    
    public void GerarProva(String saida,String periodo) throws SQLException{
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            String query = "SELECT * FROM disciplina_formatacao WHERE formatacao IS NOT null AND periodos_periodo = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1, periodo);
            ResultSet disciplinastxt = ps.executeQuery();
                        
            while(disciplinastxt.next()){
                
                byte [] bytes = disciplinastxt.getBytes("formatacao");
     		File materia = new File(saida+"\\"+disciplinastxt.getString("disciplinas_codigo")+".docx");
                FileOutputStream fos = new FileOutputStream(materia);
                fos.write( bytes );
                fos.close(); 
                
                txts.add(saida+"\\"+disciplinastxt.getString("disciplinas_codigo")+".docx");
            }
            
            
            //Lista todos os alunos no banco (nome, matricula, turma)
            query =   "SELECT distinct usuarios.nome, usuarios.matricula, usuarios.turma "
                    + "FROM usuarios "
                    + "inner join usuario_disciplina on usuarios.matricula = usuario_disciplina.usuarios_matricula "
                    + "and usuario_disciplina.periodos_periodo = ? "
                    + "WHERE usuarios.matricula <> ?";
            
            ps = c.prepareStatement(query);
            ps.setString(1, periodo);
            ps.setString(2, "admin");
            ResultSet alunos = ps.executeQuery();
            
            while(alunos.next()){
                
                ps = c.prepareStatement(
                        "select usuario_disciplina.periodos_periodo,"
                                + "usuario_disciplina.disciplinas_codigo, "
                                + "disciplinas.nome as disciplina,usuario_disciplina.usuarios_matricula, "
                                + "usuarios.nome as aluno, "
                                + "usuarios.matricula,"
                                + "disciplinas.ordem as ordem "
                        + "from usuario_disciplina "
                                + "inner join usuarios on usuario_disciplina.usuarios_matricula = usuarios.matricula "
                                + "inner join disciplinas on disciplinas.codigo = usuario_disciplina.disciplinas_codigo "
                        + "where usuarios.matricula = ? and usuario_disciplina.periodos_periodo = ? order by disciplinas.ordem");
                
                ps.setString(1, alunos.getString("matricula"));
                ps.setString(2, periodo);
                ResultSet disciplinasAlunos = ps.executeQuery();
                                                
                numeracao = 11;
                boolean entrada = true;
                boolean cccg = false;
                provaFinal = new ArrayList<>();
                provas = new ArrayList<>();
                
                //Informações do aluno pra colocar na prova/gabarito
                Capa capa = new Capa();
                capa.gerar(saida, alunos.getString("nome").toUpperCase());
                
                Gabarito gabarito = new Gabarito();
                gabarito.gerar(saida, alunos.getString("nome").toUpperCase());
                
                GabaritoMaior gabarito2 = new GabaritoMaior();
                gabarito2.gerar(saida, alunos.getString("nome").toUpperCase());
                
                Discursivas discursivas = new Discursivas();
                discursivas.gerar(saida, alunos.getString("nome").toUpperCase());
                
                while(disciplinasAlunos.next()){
                    if(disciplinasAlunos.getString("disciplinas_codigo").equals("CCCG")){
                        cccg = true;
                        
                        converter(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+".docx",saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf");
                        String resposta = new RetiraPrimeiraPagina().saida(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf",saida,disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinalizada");
                        //provaFinal.add(resposta);

                        provas.add(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf");
                        provas.add(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"NumeradaFinalizada.pdf");
                        
                    }else{
                        
                        // Verifica se a disciplian é do 1º ou 2º dia
                        if( (Integer.parseInt((disciplinasAlunos.getString("ordem"))) >= 12) && entrada){
                            numeracao = 3;
                            entrada = false;
                        }
                            
                        
                        find_replace_in_DOCX(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+".docx",saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numerada.docx");
                        provas.add(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numerada.docx");

                        converter(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numerada.docx",saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf");
                        String resposta = new RetiraPrimeiraPagina().saida(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf",saida,disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinalizada");
                        provaFinal.add(resposta);

                        provas.add(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf");
                        provas.add(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"NumeradaFinalizada.pdf");
                    }
                }
                
                if(cccg){
                    ArrayList<String> provasTemp = new ArrayList<>();
                    provasTemp.add(saida+"\\CCCGNumeradaFinalizada.pdf");
                    for(int i=0;i<provaFinal.size();i++){
                        provasTemp.add(provaFinal.get(i));
                    }
                    provaFinal = new ArrayList<>();
                    for(int i=0;i<provasTemp.size();i++){
                        provaFinal.add(provasTemp.get(i));
                    }
                }
               
                new MergePDF().saida(provaFinal, saida, alunos.getString("nome"),alunos.getString("turma"));
                                
            }
                                                
            for(int i=0;i<txts.size();i++){
                File excluir = new File(txts.get(i));
                excluir.delete();
            }
            
            for(int i=0;i<provas.size();i++){
                File excluir = new File(provas.get(i));
                excluir.delete();
            }
            
            ps.close();
            c.close();
            
            JOptionPane.showMessageDialog(null,"Gerado com Sucesso");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
        
    }
    
    public void find_replace_in_DOCX(String entrada, String saida) throws IOException,InvalidFormatException,org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        
        try {
            XWPFDocument doc = new XWPFDocument(OPCPackage.open(entrada));
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.contains("§")) {
                            text = text.replace("§", "QUESTÃO "+numeracao);
                            numeracao++;
                            r.setText(text, 0);                            
                        }                      
                    }
                }
            }
            
            doc.write(new FileOutputStream(saida));
            
        }finally{}

    }
    
    public void converter(String docx,String pdf){
        com.aspose.words.Document doc;
        try {
            doc = new com.aspose.words.Document(docx);
            doc.save(pdf);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        File excluir = new File(docx);
        excluir.delete();
    }  
    
    private byte[] toPdf(ByteArrayOutputStream docx) throws IOException {
        InputStream isFromFirstData = new ByteArrayInputStream(docx.toByteArray());

        XWPFDocument document = new XWPFDocument(isFromFirstData);
        PdfOptions options = PdfOptions.create();

        //make new file in c:\temp\
        OutputStream out = new FileOutputStream(new File("c:\\tmp\\HelloWord.pdf"));
        PdfConverter.getInstance().convert(document, out, options);

        //return byte array for return in http request.
        ByteArrayOutputStream pdf = new ByteArrayOutputStream();
        PdfConverter.getInstance().convert(document, pdf, options);

        document.write(pdf);
        document.close();
        return pdf.toByteArray();
    }
    
}
