package Principal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class GerarProvaDuasColunas {
    
    public static int numeracao = 3;
    
    public static BufferedReader lerArq = null;
    private ArrayList<String> provas = new ArrayList<>();
    
    public int repeticao = 1;    
    public static boolean precisa_mais = false;
    
    public String gerar(String caminhodisciplina,String saida,String nome) {

        try{
            File arq = new File(caminhodisciplina);
            lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(arq), "UTF-8"));
            String linha = lerArq.readLine();
            
            XWPFDocument document = new XWPFDocument(new FileInputStream(System.getProperty("user.dir")+"/src/resources/ModeloCG_CE.docx"));
                                   
            XWPFParagraph nomeDisciplina = document.createParagraph();
            nomeDisciplina.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun nomeDisc = nomeDisciplina.createRun();
            nomeDisc.setText(nome);
            nomeDisc.setBold(true);
            
            XWPFParagraph numeroQuestao1 = document.createParagraph();
            XWPFRun numeroQuestaoTexto1 = numeroQuestao1.createRun();
            numeroQuestaoTexto1.setText("Questão "+numeracao);
            numeroQuestaoTexto1.setBold(true);
            
                while(linha != null) {
                                        
                    if(linha.equals("$")){
                        
                        FileOutputStream fileOutput = new FileOutputStream(new File(saida+"\\Prova.docx"));
                        document.write(fileOutput);
                        converter(saida+"\\Prova.docx",saida+"\\"+nome+repeticao+"final.pdf"); 

                        document = new XWPFDocument(new FileInputStream(System.getProperty("user.dir")+"/src/resources/ModeloCG_CE.docx")); 

                        ArrayList provaFinalizada = new ArrayList<>();
                        provaFinalizada.add(saida+"\\"+nome+repeticao+"final.pdf");
                        String resposta = new RetiraPrimeiraPagina().saida(provaFinalizada,saida,nome+repeticao);  
                        File excluir = new File(saida+"\\"+nome+repeticao+"final.pdf");
                        excluir.delete();                       
                        provas.add(resposta);
                        
                        GerarProvaUmaColuna prova = new GerarProvaUmaColuna();
                        provas.add(prova.gerar(saida, nome+"1COLUNA"+repeticao,lerArq,nome));
                        repeticao++;
                                                                        
                        linha = lerArq.readLine();
                        
                        if(linha != null){
                            XWPFParagraph numeroQuestao = document.createParagraph();
                            XWPFRun numeroQuestaoTexto = numeroQuestao.createRun();
                            numeroQuestaoTexto.addBreak();
                            numeroQuestaoTexto.setText("Questão "+GerarProvaDuasColunas.numeracao);
                            numeroQuestaoTexto.setBold(true);
                        }
                        
                    }else{
                    
                    if(linha.substring(0,1).equals("'")){
                        if(linha.substring(1,2).equals("{")){
                            XWPFParagraph paragrafo = document.createParagraph();
                            XWPFRun texto = paragrafo.createRun();
                            
                            if(linha.substring(2,3).equals("|")){
                                if(linha.substring(3,4).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(4, linha.length()-2));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(3, linha.length()-2));
                                }
                            }else if(linha.substring(2,3).equals("/")){
                                if(linha.substring(3,4).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(4, linha.length()-2));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(3, linha.length()-2));
                                }
                            }else{
                                paragrafo.setAlignment(ParagraphAlignment.LEFT);
                                if(linha.substring(2,3).equals("!")){
                                    texto.setText(linha.substring(3, linha.length()-2));
                                    texto.setItalic(true);
                                }else{
                                    texto.setText(linha.substring(2, linha.length()-2));
                                }
                            }
                            
                            texto.setBold(true);
                                                                                    
                        }else{
                            XWPFParagraph paragrafo = document.createParagraph();
                            XWPFRun texto = paragrafo.createRun();
                            
                            if(linha.substring(1,2).equals("|")){
                                if(linha.substring(2,3).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(3, linha.length()-1));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(2, linha.length()-1));
                                }
                            }else if(linha.substring(1,2).equals("/")){
                                if(linha.substring(2,3).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(3, linha.length()-1));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(2, linha.length()-1));
                                }
                            }else{
                                paragrafo.setAlignment(ParagraphAlignment.LEFT);
                                if(linha.substring(1,2).equals("!")){
                                    texto.setText(linha.substring(2, linha.length()-1));
                                    texto.setItalic(true);
                                }else{
                                    texto.setText(linha.substring(1, linha.length()-1));
                                }
                            }
                            
                        }
                        
                        linha = lerArq.readLine();
                        
                    }else{
                        if(linha.equals("*")){
                            XWPFParagraph espaco = document.createParagraph();
                            XWPFRun espacoTexto = espaco.createRun();
                            espacoTexto.addBreak();
                            linha = lerArq.readLine();
                        }else if(linha.equals("-")){
                            numeracao++;
                            linha = lerArq.readLine();
                            linha = lerArq.readLine();
                            
                            if(linha != null && !linha.equals("$")){
                                XWPFParagraph numeroQuestao = document.createParagraph();
                                XWPFRun numeroQuestaoTexto = numeroQuestao.createRun();
                                numeroQuestaoTexto.addBreak();
                                numeroQuestaoTexto.setText("Questão "+numeracao);
                                numeroQuestaoTexto.setBold(true);
                            }

                        }else{
                            if(linha.substring(0, 2).equals("--") && !linha.substring(0, 3).equals("---")){
                                int largura = 0,altura = 0;
                                int indiceLargura = 2;
                                while(!linha.substring(indiceLargura, indiceLargura+1).equals("W")){
                                    indiceLargura++;
                                }

                                largura = Integer.parseInt(linha.substring(2, indiceLargura));
                                
                                int indiceAltura = indiceLargura;
                                while(!linha.substring(indiceAltura, indiceAltura+1).equals("H")){
                                    indiceAltura++;
                                }
                                altura = Integer.parseInt(linha.substring(indiceLargura+1, indiceAltura));
                                
                                InputStream is = new FileInputStream(linha.substring(indiceAltura+1, linha.length())); 
                                XWPFParagraph imagens = document.createParagraph();
                                imagens.setAlignment(ParagraphAlignment.LEFT);
                                XWPFRun img = imagens.createRun();
                                img.addPicture(is, Document.PICTURE_TYPE_JPEG, "", Units.toEMU(largura), Units.toEMU(altura));
                                
                                linha = lerArq.readLine();
                            }else if(linha.substring(0, 3).equals("---")){
                                int largura = 0,altura = 0;
                                int indiceLargura = 3;
                                while(!linha.substring(indiceLargura, indiceLargura+1).equals("W")){
                                    indiceLargura++;
                                }

                                largura = Integer.parseInt(linha.substring(3, indiceLargura));
                                
                                int indiceAltura = indiceLargura;
                                while(!linha.substring(indiceAltura, indiceAltura+1).equals("H")){
                                    indiceAltura++;
                                }
                                altura = Integer.parseInt(linha.substring(indiceLargura+1, indiceAltura));
                                                                
                                InputStream is = new FileInputStream(linha.substring(indiceAltura+1, linha.length())); 
                                XWPFParagraph imagens = document.createParagraph();
                                imagens.setAlignment(ParagraphAlignment.RIGHT);
                                XWPFRun img = imagens.createRun();
                                img.addPicture(is, Document.PICTURE_TYPE_JPEG, "", Units.toEMU(largura), Units.toEMU(altura));
                                
                                linha = lerArq.readLine();
                            }else{
                                
                                int largura = 0,altura = 0;
                                int indiceLargura = 0;
                                while(!linha.substring(indiceLargura, indiceLargura+1).equals("W")){
                                    indiceLargura++;
                                }

                                largura = Integer.parseInt(linha.substring(0, indiceLargura));
                                
                                int indiceAltura = indiceLargura;
                                while(!linha.substring(indiceAltura, indiceAltura+1).equals("H")){
                                    indiceAltura++;
                                }
                                altura = Integer.parseInt(linha.substring(indiceLargura+1, indiceAltura));
                                
                                
                                InputStream is = new FileInputStream(linha.substring(indiceAltura+1, linha.length())); 
                                XWPFParagraph imagens = document.createParagraph();
                                imagens.setAlignment(ParagraphAlignment.CENTER);
                                XWPFRun img = imagens.createRun();
                                img.addPicture(is, Document.PICTURE_TYPE_JPEG, "", Units.toEMU(largura), Units.toEMU(altura));
                                
                                linha = lerArq.readLine();
                            }
                            
                        }
                    }
                    
                    //linha = lerArq.readLine();
                    
                    }
                    
                }
                
                if(!GerarProvaUmaColuna.chegouNoFinal){
                    provas.add(saida+"\\"+nome+"final.pdf");
                }
                GerarProvaUmaColuna.chegouNoFinal = false;
                
                
                boolean achou = false;
                for(int i=0;i<provas.size();i++){
                    if(provas.get(i).equals(saida+"\\"+nome+"final.pdf")){
                        achou = true;
                    }
                }
                
                if(achou){
                    XWPFParagraph paragrafo = document.createParagraph();
                    XWPFRun texto = paragrafo.createRun();
                    texto.addBreak();
                    //numeracao++;
                    lerArq.close();

                    FileOutputStream fileOutput = new FileOutputStream(new File(saida+"\\Prova.docx"));
                    document.write(fileOutput);

                    converter(saida+"\\Prova.docx",saida+"\\"+nome+"final.pdf");
                    
                    
                    ArrayList<String> provafinalizada = new ArrayList<>();
                    provafinalizada.add(saida+"\\"+nome+"final.pdf");
                    String resposta = new RetiraPrimeiraPagina().saida(provafinalizada,saida,nome+"finalizada");
                    File excluir = new File(provas.get(provas.size()-1));
                    excluir.delete();
                    provas.remove(provas.size()-1);
                    provas.add(resposta);
                }
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        String retorno = provas.get(0);
         
        if(provas.size() > 1){            
            new MergePDF().saida(provas,saida,nome);
            retorno = saida+"\\"+nome+".pdf";
        }
        
        if((numeracao-1) > 40){
            precisa_mais = true;
        }
        
        return retorno;
                
    }

    public void converter(String docx,String pdf){
        com.aspose.words.Document doc;
        try {
            doc = new com.aspose.words.Document(docx);
            doc.save(pdf);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        File excluir = new File(docx);
        excluir.delete();
    }   
    
}
