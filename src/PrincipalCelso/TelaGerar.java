package PrincipalCelso;

import Telas.Disciplinas;
import static PrincipalEduardo.Janela.janela0;;
import static Telas.Disciplinas.area;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import query.Executar;

public class TelaGerar {
    private ImageIcon iconePag = new ImageIcon(getClass().getResource("/Imagens/Pagina.png"));
    private ImageIcon iconeBarraLateral = new ImageIcon(getClass().getResource("/Imagens/BarraLateral.png"));
    private ImageIcon iconeApagar = new ImageIcon(getClass().getResource("/Imagens/Apagar.png"));
    private ImageIcon iconeApagarAC = new ImageIcon(getClass().getResource("/Imagens/Apagar AC.png"));
    private ImageIcon iconeApagarPe = new ImageIcon(getClass().getResource("/Imagens/ApagarPeq.png"));
    private ImageIcon iconeApagarPeAC = new ImageIcon(getClass().getResource("/Imagens/ApagarPeqAC.png"));
    private ImageIcon iconeSconteudo = new ImageIcon(getClass().getResource("/Imagens/ImagemSconteudo.png"));
    private ImageIcon iconeSconteudoAC = new ImageIcon(getClass().getResource("/Imagens/ImagemSconteudoAC.png"));
    private ImageIcon iconeApagarPeq = new ImageIcon(getClass().getResource("/Imagens/ApagarPeq.png"));
    private ImageIcon iconeBranco = new ImageIcon(getClass().getResource("/Imagens/Branco.png"));
    private ImageIcon iconeSetaEsq = new ImageIcon(getClass().getResource("/Imagens/SetaEsq.png"));
    private ImageIcon iconeSetaEsqAC = new ImageIcon(getClass().getResource("/Imagens/SetaEsqAC.png"));
    private ImageIcon iconeSetaDir = new ImageIcon(getClass().getResource("/Imagens/SetaDir.png"));
    private ImageIcon iconeSetaDirAC = new ImageIcon(getClass().getResource("/Imagens/SetaDirAC.png"));
    private ImageIcon iconeSetaCen = new ImageIcon(getClass().getResource("/Imagens/SetaCen.png"));
    private ImageIcon iconeSetaCenAC = new ImageIcon(getClass().getResource("/Imagens/SetaCenAC.png"));
    private ImageIcon iconeNegrito = new ImageIcon(getClass().getResource("/Imagens/negrito.png"));
    private ImageIcon iconeNegritoAC = new ImageIcon(getClass().getResource("/Imagens/negritoAC.png"));
    private Icon volatil;
    
    private int numPag = 0, numQuest = 0, questClick = 0, pagTemp = 0,questTemp = 0, pagClick = 0, trava = 0, numeroBranco = 0, numeroAlt = 0, maximoDeQuestoes = 0;
    private JLabel mostrarPag, imagTam, quantCol;
    private MouseListener ml, ml2, ml3, ml4, ml5;
    private JScrollPane jsp;
    private JLabel numeroPag;
    private ArrayList<Pagina> papeis;
    private ArrayList<JPanel> areas;
    private ArrayList<Questao> conteudos;
    private ArrayList<ImagEsq> brancos;
    private ArrayList<Alternativa> alter;
    private JButton novaPag, escreve, printa,  salvar, alternativas, imaCen, buttonNegrito;
    private JTextArea textCfg;
    private JTextField tam;
    private Imagem imagCfg, temp;
    private JFileChooser fImagem = null;
    private JTabbedPane tabbed;
    private BufferedWriter writer;
    
    private JFrame janela;
    private JPanel panel;
    private JButton outraImg;
    private JPanel configurar, configurar2, configurar3;
    private static String fileTemp;
    private QuebraDpag quebra;
    private JRadioButton jrb1, jrb2;
    
    public void gerar(int numMaxQuest){
        this.maximoDeQuestoes = numMaxQuest;
        janela = new JFrame();
        janela.setTitle("Gerar modelo de prova EDEF");
        janela.setSize(iconePag.getIconWidth() + 245,iconePag.getIconHeight() - 200);   //(962,700)
        janela.setResizable(false);
        janela.setLayout(null);
        janela.setLocationRelativeTo(null);
        janela.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        janela.addWindowListener( new WindowAdapter( ){
            public void windowClosing(WindowEvent w){
                int i = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja concelar a operação de edição?\nTodos os ajustes feitos serão perdidos!", "Fechar editor", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if(i == 0){
                    janela.dispose();
                    janela0.setVisible(true);
                }
            }
        });
        mouse();
        
        imagTam = new JLabel("Tamanho:");
        imagTam.setFont(new Font("TIMES_ROMAN", Font.BOLD, 12));
        tam = new JTextField();
        tam.setColumns(5);
        tam.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                int valorTamanho = 0;
                try{
                    if (fImagem != null) {
                        if (!tam.getText().isEmpty()) {
                            valorTamanho = Integer.parseInt(tam.getText());
                            if(valorTamanho > 0){
                                imagCfg.setIcon(geraThumbnail(new ImageIcon(fImagem.getSelectedFile()+"") , valorTamanho));
                                imagTam.setForeground(Color.black);
                            }else{
                                textCfg.setForeground(Color.red);
                                imagTam.setForeground(Color.red);
                                textCfg.setText("O Tamanho só pode\nser inteiro positivo.");
                            }
                        }else{
                            imagTam.setForeground(Color.black);
                            imagCfg.setIcon(geraThumbnail(new ImageIcon(fImagem.getSelectedFile()+"") , 165));
                        }
                        imagCfg.setVisible(true);
                        volatil = (ImageIcon) imagCfg.getIcon();
                    }
                    
                }catch(NumberFormatException ex){
                    imagCfg.setIcon(geraThumbnail(iconeSconteudo , 160));
                    textCfg.setForeground(Color.red);
                    imagTam.setForeground(Color.red);
                    textCfg.setText("Só utilize inteiros\nna caixa 'Tamanho'");
                }
            }
        });
        
        outraImg = new JButton("Buscar imagem");
        outraImg.setForeground(Color.black);
        outraImg.setBounds(10,20,130,20);
        
        textCfg = new JTextArea("   ");
        textCfg.setLineWrap(true);
        textCfg.setWrapStyleWord(true);
        textCfg.setBackground(Color.LIGHT_GRAY);
        textCfg.setBounds(15,40,120,40);
        textCfg.setFont(new Font("TIMES_ROMAN", Font.BOLD, 12));
        textCfg.setEditable(false);
        
        imagCfg = new Imagem(false);
        imagCfg.setToolTipText("Arraste até o destino");
        imagCfg.addMouseListener(ml4);
        imagCfg.setIcon(null);
        imagCfg.setVisible(false);
        arrastar();
        janela.add(imagCfg);
        
        JPanel panelLateral = new JPanel(null);
        panelLateral.setBounds(iconePag.getIconWidth() + 25, 0, 220, 700);
        panelLateral.setBackground(new Color(142,179,207));
        janela.add(panelLateral);
        
        configurar = new JPanel();
        configurar.setBounds(5, 10, 180, 300);
        configurar.setBackground(Color.LIGHT_GRAY);
        configurar.setBorder(new LineBorder(Color.WHITE,3));
        configurar.setVisible(true);
        panelLateral.add(configurar);
        
        configurar.add(imagTam);
        configurar.add(tam);
        configurar.add(outraImg);
        configurar.add(textCfg);
        
        configurar2 = new JPanel();
        configurar2.setBounds(765, 10, 180, 300);
        configurar2.setBackground(Color.LIGHT_GRAY);
        configurar2.setBorder(new LineBorder(Color.WHITE,3));
        configurar2.setVisible(true);
        
        configurar3 = new JPanel();
        configurar3.setBounds(765, 10, 180, 300);
        configurar3.setBackground(Color.LIGHT_GRAY);
        configurar3.setVisible(true);
        configurar3.setLayout(new GridLayout(0,1,0,12));
        
        for (int i = 0; i < numMaxQuest; i++) {
            Box box = Box.createHorizontalBox();
            JLabel nn = new JLabel((i+1)+"):");
            nn.setFont(new Font("TIMES_ROMAN", Font.BOLD, 15));
            
            ButtonGroup grupo = new ButtonGroup();  //Permite que só um RadioButton seja marcado por vez em uma sequencia
            JRadioButton aa = new JRadioButton("a)");grupo.add(aa);aa.setToolTipText("(Questão "+(i+ 1)+"): letra (a)");
            JRadioButton bb = new JRadioButton("b)");grupo.add(bb);bb.setToolTipText("(Questão "+(i+ 1)+"): letra (b)");
            JRadioButton cc = new JRadioButton("c)");grupo.add(cc);cc.setToolTipText("(Questão "+(i+ 1)+"): letra (c)");
            JRadioButton dd = new JRadioButton("d)");grupo.add(dd);dd.setToolTipText("(Questão "+(i+ 1)+"): letra (d)");
            JRadioButton ee = new JRadioButton("e)");grupo.add(ee);ee.setToolTipText("(Questão "+(i+ 1)+"): letra (e)");
            
            bb.setBackground(Color.white);
            dd.setBackground(Color.white);
            
            box.add(nn);
            box.add(aa);
            box.add(bb);
            box.add(cc);
            box.add(dd);
            box.add(ee);
            configurar3.add(box);
        }
        JScrollPane jsp2 = new JScrollPane(configurar3);
        jsp2.getVerticalScrollBar().setUnitIncrement(5);
        jsp2.setBounds(765, 10, 180, 300);
        
        tabbed = new JTabbedPane();
        escreve= new JButton("Questão");
        escreve.setToolTipText("Adiciona Questão");
        
        imaCen = new JButton("Imagem");
        imaCen.setForeground(Color.black);
        
        imaCen.setToolTipText("Coloca Imagem");
        
        alternativas = new JButton("Alternativas");
        alternativas.setToolTipText("Gerar Alternativas");
        
        configurar2.add(escreve);
        configurar2.add(imaCen);
        configurar2.add(alternativas);
        
        tabbed.add("Inserir",configurar2);
        tabbed.add("Gabarito",jsp2);
        tabbed.setBounds(5, 310, 175, 200);
        panelLateral.add(tabbed);
        
        printa = new JButton("Printar Questões");
        printa.setForeground(Color.black);
        printa.setBounds(20, 515, 140, 20);
        //panelLateral.add(printa);
        
        quantCol = new JLabel("Colunas:");
        quantCol.setBounds(30,540,100,20);
        panelLateral.add(quantCol);
        
        jrb1 = new JRadioButton("1");
        jrb1.setBounds(85,540,40,20);
        jrb1.setToolTipText("1 Coluna");
        jrb1.setBackground(null);
        panelLateral.add(jrb1);
        
        jrb2 = new JRadioButton("2");
        jrb2.setBounds(120,540,40,20);
        jrb2.setToolTipText("2 Colunas");
        jrb2.setBackground(null);
        jrb2.setSelected(true);
        panelLateral.add(jrb2);
        
        ButtonGroup grupoColunas = new ButtonGroup();
        grupoColunas.add(jrb1);
        grupoColunas.add(jrb2);
        
        novaPag = new JButton("Nova Pagina");
        novaPag.setForeground(Color.black);
        novaPag.setToolTipText("Adiciona Uma Página");
        novaPag.setBounds(30,560,120,20);
        panelLateral.add(novaPag);
        
        mostrarPag = new JLabel("   ");
        mostrarPag.setBounds(35,585,130,20);
        panelLateral.add(mostrarPag);
        
        salvar = new JButton("Salvar");
        salvar.setForeground(Color.black);
        salvar.setToolTipText("Salvar Arquivo");
        salvar.setBounds(40,610,100,20);
        panelLateral.add(salvar);
        
        panel = new JPanel();
        panel.setBackground(null);
        panel.setLayout(new BoxLayout (panel, BoxLayout.Y_AXIS));
        
        jsp = new JScrollPane(panel);
        jsp.getVerticalScrollBar().setUnitIncrement(25);
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        papeis = new ArrayList();
        areas = new ArrayList();
        conteudos = new ArrayList();
        brancos = new ArrayList();
        alter = new ArrayList();
        
        jsp.setBounds(0, 0, iconePag.getIconWidth()+24, iconePag.getIconHeight() - 230);
        janela.add(jsp);
        
        todosOsBotoes();
        quebra = new QuebraDpag();
        quebra.start();
        
        janela.setVisible(true);
        
    }
    
    public ImageIcon geraThumbnail(ImageIcon arquivo, int tamanho){
        ImageIcon thumbnail = null;
        int tamanhoLargura = arquivo.getIconWidth(), tamanhoAltura = arquivo.getIconHeight();
        if (tamanhoLargura > tamanhoAltura) {
            if(arquivo.getIconWidth() > tamanho){
                thumbnail = new ImageIcon(
                arquivo.getImage().getScaledInstance(tamanho, -1, Image.SCALE_DEFAULT));
            } else{
                thumbnail = arquivo;
            }
        }else{
            if(arquivo.getIconHeight() > tamanho){
                thumbnail = new ImageIcon(
                arquivo.getImage().getScaledInstance(-1, tamanho, Image.SCALE_DEFAULT));
            } else{
                thumbnail = arquivo;
            }
        }
        return thumbnail;
    }
    
    public ImageIcon diminuiImagem(ImageIcon arquivo, int larg){
        ImageIcon thumbnail = null;
        if(arquivo.getIconWidth() > larg){
            thumbnail = new ImageIcon(
            arquivo.getImage().getScaledInstance(larg, arquivo.getIconHeight(), Image.SCALE_SMOOTH));
        } else{
            thumbnail = arquivo;
        }
        return thumbnail;
    }
    
    public int[] respostas(){
        int[] resultado = new int[maximoDeQuestoes];
        for (int i = 0; i < maximoDeQuestoes; i++) {
            if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(1)).isSelected()){
                resultado[i] = 1;   //letra a
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(2)).isSelected()){
                resultado[i] = 2;   //letra b
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(3)).isSelected()){
                resultado[i] = 3;   //letra c
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(4)).isSelected()){
                resultado[i] = 4;   //letra d
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(5)).isSelected()){
                resultado[i] = 5;   //letra e
            }else{
                resultado[i] = 6;   // sem alternativas marcadas
            }
        }
        return resultado;
    }
    
    public void pintaQuestoes(){
        for (int i = 0; i < respostas().length; i++) {
            if(respostas()[i] == 6){
                ((JLabel)((Box)configurar3.getComponent(i)).getComponent(0)).setForeground(Color.red);//Pinta de vermelho as questões não marcadas
            }else{
                ((JLabel)((Box)configurar3.getComponent(i)).getComponent(0)).setForeground(Color.black);//Pinta de preto as questões marcadas
            }
        }
    }
    
    
    
    public int pagMaiorQuest(){
        int quantAreas = 0;//quantidade de areas
        int numeroPag = 0, numeroMaior = 0;
        for (int i = 0; i < papeis.size(); i++) {
            if (numeroMaior < papeis.get(i).getNumUltimaQuest()) {
                numeroMaior = papeis.get(i).getNumUltimaQuest();
                numeroPag = i+1;
            }
        }
        return numeroPag;
    }
    
    public void ajustaNumQuest(){
        for (int i = 0; i < conteudos.size(); i++) {
            conteudos.get(i).mudarTexto(i + 1);
        }
    }
    
    public int areaDaPagAtual(int pag){ //Iserir valor registrado no ArrayList 'papeis'
        int quantAreasAteAtual = 0;
        for (int i = 0; i < pag; i++) {
            if(papeis.get(i).getColunas() == 1){
                quantAreasAteAtual++;
            }else if(papeis.get(i).getColunas() == 2){
                quantAreasAteAtual+=2;
            }
        }
        return quantAreasAteAtual;  // retorna o valor de arrayList referente à primeira area da pagina que está sendo pedida.
    }
    
    public void criarQuest(){
        int reg = numQuest;
        if(numQuest == 1){      //Se a questão é a unica da prova até o momento
            if(papeis.get(pagTemp - 1).getColunas() == 1){
                conteudos.add(new Questao(reg, 1));
            }else if(papeis.get(pagTemp - 1).getColunas() == 2){
                conteudos.add(new Questao(reg, 2));
            }
        }else{
            if(papeis.get(pagClick - 1).getNumUltimaQuest() != 0){  //Se tem alguma questão na página
                reg = papeis.get(pagClick - 1).getNumUltimaQuest() + 1; //O número da questão será o ultimo número registrado na página atual + 1
            }else{
                reg = 1;
                for(int i = 0; i < (pagClick-1); i++){ //acha o maior ultimo número registrado em páginas anteriores
                    if(papeis.get(i).getNumUltimaQuest() != 0){
                        reg = papeis.get(i).getNumUltimaQuest() + 1; //O número da questão será o ultimo da página anterior + 1
                    }
                }
            }
            if(numPag > pagClick){  //Se a página clicada não for a ultima
                int pagUltimoNum = pagMaiorQuest(); //Numero da pagina que tem a maior quest
                if(pagUltimoNum > pagClick){    //Se a página referente a questão de maior número for maior que a página clicada
                    if(papeis.get(pagTemp - 1).getColunas() == 1){
                        conteudos.add(reg - 1,new Questao(reg, 1));
                    }else if(papeis.get(pagTemp - 1).getColunas() == 2){
                        conteudos.add(reg - 1,new Questao(reg, 2));
                    }
                    ajustaNumQuest();   //adapta o valor de todas as questões, para que o numero delas fique em ordem crescente
                }else{
                    if(papeis.get(pagTemp - 1).getColunas() == 1){
                        conteudos.add(new Questao(reg, 1));
                    }else if(papeis.get(pagTemp - 1).getColunas() == 2){
                        conteudos.add(new Questao(reg, 2));
                    }
                }
            }else{  //Se a página clicada for a ultima
                if(papeis.get(pagTemp - 1).getColunas() == 1){
                    conteudos.add(new Questao(reg, 1));
                }else if(papeis.get(pagTemp - 1).getColunas() == 2){
                    conteudos.add(new Questao(reg, 2));
                }
            }
        }
        if(papeis.get(pagClick - 1).getColunas() == 1){ //Se a página é de uma coluna
            areas.get(areaDaPagAtual(pagClick - 1)).add(conteudos.get(reg - 1));
        }else if(papeis.get(pagClick - 1).getColunas() == 2){   //Se a Página é de duas colunas
            if(areas.get(areaDaPagAtual(pagClick - 1) + 1).getComponents().length > 0){ //Se já houver componentes na segunda coluna da página
                areas.get(areaDaPagAtual(pagClick - 1) + 1).add(conteudos.get(reg - 1));
            }else{
                areas.get(areaDaPagAtual(pagClick - 1)).add(conteudos.get(reg - 1));
            }
        }
        ajustaMaiorMenor(); //ajusta o valor de todas as ultimas questões e primeiras questões de cada página
        conteudos.get(reg - 1).setTabSize(5);
        
        conteudos.get(reg - 1).addMouseListener(ml3);
        conteudos.get(reg - 1).getApag().addMouseListener(ml2);
        conteudos.get(reg - 1).getNegri().addMouseListener(ml2);
    }
    
    public void criarPagDuasColu(){
        areas.add(new JPanel());
        areas.get(areas.size() - 1).setLayout (new FlowLayout(FlowLayout.LEFT));
        areas.get(areas.size() - 1).setBounds(46,100,245,735);
        areas.get(areas.size() - 1).setBackground(Color.white);
        
        areas.add(new JPanel());
        areas.get(areas.size() - 1).setLayout (new FlowLayout(FlowLayout.LEFT));
        areas.get(areas.size() - 1).setBounds(330,100,245,735);
        areas.get(areas.size() - 1).setBackground(Color.white);
        
        papeis.add(new Pagina(numPag, 2));
        panel.add(papeis.get(numPag - 1));
        papeis.get(numPag - 1).addMouseListener(ml);
        papeis.get(numPag - 1).setToolTipText("Página: "+numPag);
        papeis.get(numPag - 1).getImageApagar().addMouseListener(ml2);
        
        papeis.get(numPag - 1).add(areas.get(areas.size() - 2));    //Primeiro adiciona a penultima
        papeis.get(numPag - 1).add(areas.get(areas.size() - 1));    //Depois adiciona a Ultima
    }
    
    public void criarPag(){
        areas.add(new JPanel());
        areas.get(areas.size() - 1).setLayout (new FlowLayout(FlowLayout.LEFT));
        areas.get(areas.size() - 1).setBounds(46,100,530,735);
        areas.get(areas.size() - 1).setBackground(Color.white);
        
        papeis.add(new Pagina(numPag, 1));
        panel.add(papeis.get(numPag - 1));
        papeis.get(numPag - 1).addMouseListener(ml);
        papeis.get(numPag - 1).setToolTipText("Página: "+numPag);
        papeis.get(numPag - 1).getImageApagar().addMouseListener(ml2);
        
        papeis.get(numPag - 1).add(areas.get(areas.size() - 1));
    }
    
    public void criarPagMeio(int num){
        areas.add(num, new JPanel());
        areas.get(num).setLayout (new FlowLayout(FlowLayout.LEFT));
        areas.get(num).setBounds(46,100,525,735);
        areas.get(num).setBackground(Color.yellow);
        
        papeis.add(num, new Pagina(numPag, 1));
        panel.add(papeis.get(num), num);
        papeis.get(num).addMouseListener(ml);
        papeis.get(num).setToolTipText("Página: "+numPag);
        papeis.get(num).getImageApagar().addMouseListener(ml2);
        
        papeis.get(num).add(areas.get(num));
        if(pagClick > 0){
            mostrarPag.setText("Página: ("+pagClick+" de "+papeis.size()+")");
        }else{
            mostrarPag.setText("Total de páginas: "+papeis.size());
        }
        ajustaNumPag();
        ajustaMaiorMenor();
        jsp.validate();
    }
    
    public void ajustaNumPag(){
        for (int i = 0; i < papeis.size(); i++) {
            papeis.get(i).mudaInfo(i + 1);
        }
    }
    
    public void ajustaMaiorMenor(){
        int quantAreas = 0;
        for (int i = 0; i < papeis.size(); i++) {
            int vezes = 0;  //quantas vezes terá o loop pra saber qual é a maior questão e a menor questão da página atual
            int min = 0, max = 0;
            boolean prim = true;
            while(vezes < papeis.get(i).getColunas()){
                Component comps[] = (areas.get(quantAreas)).getComponents(); // retorna todos os componentes do JPanel
                for (int j = 0; j < comps.length; j++) {
                    if (comps[j] instanceof Questao) {   // verifica se é uma Questão
                        if(prim){   //Só entra na primeira vez que surgir uma questão
                            min = ((Questao)comps[j]).getQuestN();
                            prim = false;
                        }
                        max = ((Questao)comps[j]).getQuestN();
                    }
                }
                papeis.get(i).setNumPrimeiraQuest(min);
                papeis.get(i).setNumUltimaQuest(max);
                quantAreas++;
                vezes++;
            }
        }
    }
    
    public void apagaTudoDaPag(int pag){    //pag é o valor do ArrayList de papeis
        int vezes = 0;  //quantidade de repetições
        boolean temQuest = false;
        while(vezes < papeis.get(pag).getColunas()){    //repetir o procedimento pra quantas colunas existir na página
            Component comps[] = (areas.get(areaDaPagAtual(pag)+vezes)).getComponents(); // retorna todos os componentes do JPanel
            for (int j = 0; j < comps.length; j++) {
                if (comps[j] instanceof Questao) {   // verifica se é uma Questão
                    conteudos.remove(((Questao)comps[j]).getQuestN() - 1);
                    numQuest--;
                    temQuest = true;
                    ajustaNumQuest();
                    ajustaMaiorMenor();
                }else if (comps[j] instanceof ImagEsq) {
                    int numImgRem = 0;
                    for (int k = 0; k < brancos.size(); k++) {
                        if(((ImagEsq)comps[j]).getNum() == brancos.get(k).getNum()){
                            numImgRem = k;
                            break;
                        }
                    }
                    areas.get(areaDaPagAtual(pag)+vezes).remove(brancos.get(numImgRem));
                    brancos.remove(numImgRem);
                }else if (comps[j] instanceof Alternativa) {
                    int numAltRem = 0;
                    for (int k = 0; k < alter.size(); k++) {
                        if(((Alternativa)comps[j]).getIdentidade() == alter.get(k).getIdentidade()){
                            numAltRem = k;
                            break;
                        }
                    }
                    areas.get(areaDaPagAtual(pag)+vezes).remove(alter.get(numAltRem));
                    alter.remove(numAltRem);
                }
            }
            vezes++;
        }
        if(temQuest){
            boolean saiDoLoop = false;
            for (int i = pag + 1; i < papeis.size(); i++) { //apaga todo tipo de componente depois da pagina pedida até encontrar uma questão, não à apagando
                vezes = 0;
                if(!saiDoLoop){
                    while(vezes < papeis.get(i).getColunas()){
                        Component comps[] = areas.get(i).getComponents();
                        for (int j = 0; j < comps.length; j++) {
                            if (comps[j] instanceof Questao) {
                                saiDoLoop = true;
                                break;
                            }else if (comps[j] instanceof ImagEsq) {
                                int numImgRem = 0;
                                for (int k = 0; k < brancos.size(); k++) {
                                    if(((ImagEsq)comps[j]).getNum() == brancos.get(k).getNum()){
                                        numImgRem = k;
                                        break;
                                    }
                                }
                                areas.get(areaDaPagAtual(i)+vezes).remove(brancos.get(numImgRem));
                                brancos.remove(numImgRem);
                            }else if (comps[j] instanceof Alternativa) {
                                int numAltRem = 0;
                                for (int k = 0; k < alter.size(); k++) {
                                    if(((Alternativa)comps[j]).getIdentidade() == alter.get(k).getIdentidade()){
                                        numAltRem = k;
                                        break;
                                    }
                                }
                                areas.get(areaDaPagAtual(i)+vezes).remove(alter.get(numAltRem));
                                alter.remove(numAltRem);
                            }
                        }
                        vezes++;
                    }
                }else{
                    for (int j = 0; j < papeis.size(); j++) {   //ajusta todas as segundas colunas para recuarem para a primeira se possivel
                        if(papeis.get(j).getColunas() == 2){
                            ajustaSegColuna(j + 1);
                        }
                    }
                    atualizar();
                    break;
                }
            }
        }
    }
    
    public void apagarPagina(){
        apagaTudoDaPag(pagTemp - 1);
        
        panel.remove(papeis.get(pagTemp - 1));// aqui começa a remoção de páginas
        
        if(papeis.get(pagTemp - 1).getColunas() == 1){ //Se a página é de uma coluna
            areas.remove(areaDaPagAtual(pagTemp - 1));
        }else if(papeis.get(pagTemp - 1).getColunas() == 2){   //Se a página é de duas colunas
            areas.remove(areaDaPagAtual(pagTemp - 1));  //remove a segunda coluna da página primeiro
            areas.remove(areaDaPagAtual(pagTemp - 1));      //remove a primeira coluna da página por ultimo
        }
        papeis.remove(pagTemp - 1);
        pagClick = 0;
        
        for (int i = 0; i < papeis.size(); i++) {   //Deixa todos os papeis sem borda
            papeis.get(i).setBorder(new LineBorder(null));
        }
        
        if(pagTemp - 1 == 0){       //Se for a primeira página sendo apagada
            if(papeis.size() == 0){ // se não tiver nenhum outro papel
                numPag = 0;
            }else{  //Com papel sendo usado
                numPag--;
            }
        }else{
            numPag--;
        }
        ajustaNumPag();
        
        jsp.validate();
    }
    public void todosOsBotoes(){
        outraImg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                boolean aceito = false;
                for (int i = 0; i < papeis.size(); i++) {
                    if(papeis.get(i).isClicada()){
                        aceito = true;
                        break;
                    }
                }
                if(aceito){
                    textCfg.setText("   ");
                    try{
                        int valorTamanho = 0;
                        try{
                            if (!tam.getText().isEmpty()) {
                                valorTamanho = Integer.parseInt(tam.getText());
                            }
                            fImagem = new JFileChooser();
                            fImagem.setFileFilter(new FileNameExtensionFilter("Imagens", "jpg", "png"));
                            fImagem.setMultiSelectionEnabled(false);
                            fImagem.showOpenDialog(null);

                            textCfg.setForeground(Color.black);
                            textCfg.setText(fImagem.getSelectedFile().getName());
                            if (!tam.getText().isEmpty()) {
                                imagCfg.setIcon(geraThumbnail(new ImageIcon(fImagem.getSelectedFile().getPath()) , valorTamanho));
                            }else{
                                imagCfg.setIcon(geraThumbnail(new ImageIcon(fImagem.getSelectedFile().getPath()) , 165));
                            } 
                            imagCfg.setVisible(true);
                            fileTemp = fImagem.getSelectedFile().getPath();
                            volatil = (ImageIcon) imagCfg.getIcon();
                            imagCfg.setBounds( iconePag.getIconWidth() + 35, 110, 170, 160);
                        }catch(NumberFormatException ex){
                            textCfg.setForeground(Color.red);
                            textCfg.setText("Só utilize inteiros\nna caixa 'Tamanho'");
                        }
                    }catch(Exception e){
                        imagCfg.setIcon(geraThumbnail(iconeSconteudo , 160));
                        textCfg.setForeground(Color.red);
                        textCfg.setText("Houve um Problema\nao buscar a imagem!");
                    }
                }else{
                    textCfg.setForeground(Color.red);
                    if(papeis.size() == 0){
                        novaPag.setForeground(Color.red);
                        textCfg.setText("Crie uma página antes.");
                    }else{
                        textCfg.setText("Selecione uma página.");
                    }
                }
            }
        });
        
        alternativas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                boolean aceito = false;
                for (int i = 0; i < papeis.size(); i++) {
                    if(papeis.get(i).isClicada()){
                        aceito = true;
                        break;
                    }
                }
                if(aceito && conteudos.size() > 0){
                    textCfg.setText("   ");
                    numeroAlt++;
                    if(papeis.get(pagClick - 1).getColunas() == 1){
                        alter.add(new Alternativa(numeroAlt, 1));
                    }else if(papeis.get(pagClick - 1).getColunas() == 2){
                        alter.add(new Alternativa(numeroAlt, 2));
                    }
                    alter.get(alter.size() - 1).setIdentidade(numeroAlt);
                    alter.get(alter.size() - 1).addMouseListener(ml5);
                    alter.get(alter.size() - 1).getApag().addMouseListener(ml2);
                    alter.get(alter.size() - 1).getNegri().addMouseListener(ml2);
                    
                    if(papeis.get(pagClick - 1).getColunas() == 1){ //Se a página é de uma coluna
                        areas.get(areaDaPagAtual(pagClick - 1)).add(alter.get(alter.size() - 1));
                    }else if(papeis.get(pagClick - 1).getColunas() == 2){   //Se a Página é de duas colunas
                        if(areas.get(areaDaPagAtual(pagClick - 1) + 1).getComponents().length > 0){ //Se já houver componentes na segunda coluna da página
                            areas.get(areaDaPagAtual(pagClick - 1) + 1).add(alter.get(alter.size() - 1));
                        }else{
                            areas.get(areaDaPagAtual(pagClick - 1)).add(alter.get(alter.size() - 1));
                        }
                    }
                    papeis.get(pagClick - 1).setVisible(false);
                    papeis.get(pagClick - 1).setVisible(true);
                }else{
                    textCfg.setForeground(Color.red);
                        if(papeis.size() == 0){
                            tabbed.setForegroundAt(0, Color.red);
                            novaPag.setForeground(Color.red);
                            textCfg.setText("Crie uma página antes.");
                        }else{
                            if(pagClick == 0){
                                textCfg.setText("Selecione uma página.");
                            }else if(conteudos.size() == 0){
                                tabbed.setForegroundAt(0, Color.red);
                                escreve.setForeground(Color.red);
                                textCfg.setText("Crie uma questão antes.");
                            }
                        }
                    
                }
            }
        });
        
        salvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                boolean aceito = false, naoTemNula = true;
                for (int i = 0; i < papeis.size(); i++) {
                    if(papeis.get(i).isClicada()){
                        aceito = true;
                        break;
                    }
                }
                for (int i = 0; i < maximoDeQuestoes; i++) {
                    if(respostas()[i] == 6){
                        naoTemNula = false;
                    }
                }
                if(aceito && conteudos.size() > 0){
                    if(numQuest == maximoDeQuestoes){
                        if(naoTemNula){
                            textCfg.setText("   ");
                            quantResp = 0;
                            try {
                                String path = ((new File("Necessario")).getAbsolutePath()).toString()+"\\DadosDeDisciplinas.txt";   //manda o caminho do novo Txt para o programa
                                File file = new File(path); //cria o novo txt no caminho dado com o nome DadosDeDisciplinas
                                writer = new BufferedWriter(new FileWriter(file));
                                for (int i = 0; i < areas.size(); i++) {
                                    if(papeis.get(areaParaPag(i) - 1).getColunas() == 1){
                                        writer.write("$");   //Printa um $ no inicio de toda pagina que seja composta apenas de uma coluna
                                        writer.newLine();
                                    }
                                    mostrarComponentes(areas.get(i), true);
                                    if(papeis.get(areaParaPag(i) - 1).getColunas() == 1 && papeis.get(areaParaPag(i) - 1).getPagN() != papeis.get(papeis.size() - 1).getPagN()){
                                        writer.write("$");    //Printa um $ se a pagina foi de uma unica coluna e não é a ultima pagina do documento
                                        writer.newLine();
                                    }
                                }
                                writer.write("-");
                                writer.newLine();
                                writer.write(""+respostas()[respostas().length - 1]); //Printa o ultimo numero do gabarito
                                writer.newLine();
                                if(papeis.get(papeis.size() - 1).getColunas() == 1){    //Printa um $ se a ultima pag
                                    writer.write("$");
                                }
                                //Criando o conteúdo do arquivo
                                writer.flush();
                                //Fechando conexão e escrita do arquivo.
                                writer.close();
                                textCfg.setForeground(Color.red);
                                textCfg.setText("Salvo com sucesso!");
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
//                                Executar executar = new Executar();
//                                try {
//                                    
//                                    executar.formatarDisciplina(new File(System.getProperty("user.dir")+"/Necessario/DadosDeDisciplinas.txt"),codigo);
//                                } catch (Exception ex) {
//                                    JOptionPane.showMessageDialog(null, "Erro SQL");
//                                }
//                                janela.dispose();
//                                janela0.setVisible(true);
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                            } catch (IOException ex) {
                                //Logger.getLogger(TelaGerar.class.getName()).log(Level.SEVERE, null, ex);
                                textCfg.setForeground(Color.red);
                                textCfg.setText("pasta \"Necessario\"\nNão encontrada!");
                            }
                            tabbed.setBackgroundAt(1,null);
                            tabbed.setForegroundAt(1,Color.black);
                            pintaQuestoes();
                        }else{
                            textCfg.setForeground(Color.red);
                            textCfg.setText("Toda questão deve\nter uma resposta.");
                            tabbed.setBackgroundAt(1,Color.red);
                            tabbed.setForegroundAt(1,Color.white);
                            pintaQuestoes();
                        }
                    }else{
                        textCfg.setForeground(Color.red);
                        tabbed.setForegroundAt(0, Color.red);
                        escreve.setForeground(Color.red);
                        textCfg.setText("Insira o Valor\nmáximo de questões");
                    }

                }else{
                    textCfg.setForeground(Color.red);

                    if(papeis.size() == 0){
                        tabbed.setForegroundAt(0, Color.red);
                        novaPag.setForeground(Color.red);
                        textCfg.setText("Crie uma página antes.");
                    }else{
                        if(pagClick == 0){
                            textCfg.setText("Selecione uma página.");
                        }else if(conteudos.size() == 0){
                            tabbed.setForegroundAt(0, Color.red);
                            escreve.setForeground(Color.red);
                            textCfg.setText("Crie uma questão antes.");
                        }else if(!aceito){
                            textCfg.setText("Clique em uma página.");
                        }
                    }
                }
                     
            }
        });
        
        imaCen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                boolean aceito = false;
                for (int i = 0; i < papeis.size(); i++) {
                    if(papeis.get(i).isClicada()){
                        aceito = true;
                        break;
                    }
                }
                if(aceito && conteudos.size() > 0){
                    textCfg.setText("   ");
                    numeroBranco++;
                    brancos.add(new ImagEsq("CENTRO", true));
                    if(papeis.get(pagClick - 1).getColunas() == 1){
                        brancos.get(brancos.size() - 1).setIcon(aumentaImagem(iconeBranco, iconeSconteudo.getIconHeight(),635));    //Aumenta o tamanho vertical do fundo branco
                        brancos.get(brancos.size() - 1).getImp().setLocation((525 - brancos.get(brancos.size() - 1).getImp().getWidth())/2, 0);
                    }else if(papeis.get(pagClick - 1).getColunas() == 2){
                        brancos.get(brancos.size() - 1).setIcon(aumentaImagem(iconeBranco, iconeSconteudo.getIconHeight(),240));    //Aumenta o tamanho vertical do fundo branco
                        brancos.get(brancos.size() - 1).getImp().setLocation((240 - brancos.get(brancos.size() - 1).getImp().getWidth())/2, 0);
                    }
                    brancos.get(brancos.size() - 1).setNum(numeroBranco);
                    brancos.get(brancos.size() - 1).getImp().setNumero(numeroBranco);
                    brancos.get(brancos.size() - 1).getImp().addMouseListener(ml4);
                    brancos.get(brancos.size() - 1).getImp().getApag().addMouseListener(ml2);
                    brancos.get(brancos.size() - 1).getImp().getEs().addMouseListener(ml2);
                    brancos.get(brancos.size() - 1).getImp().getCe().addMouseListener(ml2);
                    brancos.get(brancos.size() - 1).getImp().getDi().addMouseListener(ml2);
                    
                    if(papeis.get(pagClick - 1).getColunas() == 1){ //Se a página é de uma coluna
                        areas.get(areaDaPagAtual(pagClick - 1)).add(brancos.get(brancos.size() - 1));
                    }else if(papeis.get(pagClick - 1).getColunas() == 2){   //Se a Página é de duas colunas
                        if(areas.get(areaDaPagAtual(pagClick - 1) + 1).getComponents().length > 0){ //Se já houver componentes na segunda coluna da página
                            areas.get(areaDaPagAtual(pagClick - 1) + 1).add(brancos.get(brancos.size() - 1));
                        }else{
                            areas.get(areaDaPagAtual(pagClick - 1)).add(brancos.get(brancos.size() - 1));
                        }
                    }
                    
                    jsp.validate();
                }else{
                    textCfg.setForeground(Color.red);
                    
                        if(papeis.size() == 0){
                            tabbed.setForegroundAt(0, Color.red);
                            novaPag.setForeground(Color.red);
                            textCfg.setText("Crie uma página antes.");
                        }else{
                            if(pagClick == 0){
                                textCfg.setText("Selecione uma página.");
                            }else if(conteudos.size() == 0){
                                tabbed.setForegroundAt(0, Color.red);
                                escreve.setForeground(Color.red);
                                textCfg.setText("Crie uma questão antes.");
                            }
                        }
                    
                }
            }
        });
        
        
        printa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                boolean aceito = false, naoTemNula = true;
                for (int i = 0; i < papeis.size(); i++) {
                    if(papeis.get(i).isClicada()){
                        aceito = true;
                        break;
                    }
                }
                for (int i = 0; i < maximoDeQuestoes; i++) {
                    if(respostas()[i] == 6){
                        naoTemNula = false;
                    }
                }
                if(aceito && conteudos.size() > 0){
                    if(numQuest == maximoDeQuestoes){
                        if(naoTemNula){
                            textCfg.setText("   ");
                            quantResp = 0;
                            for (int i = 0; i < areas.size(); i++) {
                                if(papeis.get(areaParaPag(i) - 1).getColunas() == 1){
                                    System.out.println("$");    //Printa um $ no inicio de toda pagina que seja composta apenas de uma coluna
                                }
                                mostrarComponentes(areas.get(i), false);
                                if(papeis.get(areaParaPag(i) - 1).getColunas() == 1 && papeis.get(areaParaPag(i) - 1).getPagN() != papeis.get(papeis.size() - 1).getPagN()){
                                    System.out.println("$");    //Printa um $ se a pagina foi de uma unica coluna e não é a ultima pagina do documento
                                }
                            }
                            System.out.println("-");    //Representa uma tecla enter entre um componente da folha e outro.
                            System.out.println(respostas()[respostas().length - 1]); //Printa o ultimo numero do gabarito
                            if(papeis.get(papeis.size() - 1).getColunas() == 1){    //Printa um $ se a ultima pag 
                                System.out.println("$");
                            }
                            tabbed.setBackgroundAt(1,null);
                            tabbed.setForegroundAt(1,Color.black);
                            pintaQuestoes();
                        }else{
                            textCfg.setForeground(Color.red);
                            textCfg.setText("Toda questão deve\nter uma resposta.");
                            tabbed.setBackgroundAt(1,Color.red);
                            tabbed.setForegroundAt(1,Color.white);
                            pintaQuestoes();
                        }
                    }else{
                        textCfg.setForeground(Color.red);
                        tabbed.setForegroundAt(0, Color.red);
                        escreve.setForeground(Color.red);
                        textCfg.setText("Insira o Valor\nmáximo de questões");
                    }

                }else{
                    textCfg.setForeground(Color.red);

                    if(papeis.size() == 0){
                        tabbed.setForegroundAt(0, Color.red);
                        novaPag.setForeground(Color.red);
                        textCfg.setText("Crie uma página antes.");
                    }else{
                        if(pagClick == 0){
                            textCfg.setText("Selecione uma página.");
                        }else if(conteudos.size() == 0){
                            tabbed.setForegroundAt(0, Color.red);
                            escreve.setForeground(Color.red);
                            textCfg.setText("Crie uma questão antes.");
                        }else if(!aceito){
                            textCfg.setText("Clique em uma página.");
                        }
                    }
                }
            }
        });
        
        escreve.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                boolean aceito = false;
                for (int i = 0; i < papeis.size(); i++) {
                    if(papeis.get(i).isClicada()){
                        aceito = true;
                        break;
                    }
                }
                if(aceito){
                    if(numQuest < maximoDeQuestoes){
                        tabbed.setForegroundAt(0, Color.black);
                        escreve.setForeground(Color.black);
                        textCfg.setText("   ");
                        numQuest++;
                        criarQuest();
                        papeis.get(pagClick - 1).setVisible(false); //Atualiza a pagina para o aparecimento da questão
                        papeis.get(pagClick - 1).setVisible(true);
                    }else{
                        textCfg.setForeground(Color.red);
                        textCfg.setText("Valor máximo de\nquestões atingido!");
                    }
                }else{
                    textCfg.setForeground(Color.red);
                    if(papeis.size() == 0){
                        tabbed.setForegroundAt(0, Color.red);
                        novaPag.setForeground(Color.red);
                        textCfg.setText("Crie uma página antes.");
                        
                    }else{
                        textCfg.setText("Selecione uma página.");
                    }
                }
            }
        });
        
        novaPag.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                numPag++;
                tabbed.setForegroundAt(0, Color.black);
                novaPag.setForeground(Color.black);
                if(jrb1.isSelected()){
                    criarPag();
                }else if(jrb2.isSelected()){
                    criarPagDuasColu();
                }
                if(pagClick > 0){
                    mostrarPag.setText("Página: ("+pagClick+" de "+papeis.size()+")");
                }else{
                    mostrarPag.setText("Total de páginas: "+papeis.size());
                }
                jsp.validate();
            }
        });
        
    }
    private int quantResp = 0;
    public void mostrarComponentes(JPanel obj, boolean key){    //key = true (é pra salvar); key = false (é pra printar)
        try{
            Component comps[] = obj.getComponents(); // retorna todos os componentes do JPanel
            for (int i = 0; i < comps.length; i++) {
                if (comps[i] instanceof Questao) {
                    String origem = ((Questao)comps[i]).getText();
                    if(quantResp > 0){
                        if(key){
                            writer.write("-");
                            writer.newLine();
                        }else{
                            System.out.println("-");    //Representa uma tecla enter entre um componente da folha e outro.
                        }
                        if(key){
                            writer.write(""+respostas()[quantResp - 1]);
                            writer.newLine();
                        }else{
                            System.out.println(respostas()[quantResp - 1]); //printa os resultados de cada questão
                        }
                    }
                    quantResp++;
                    if(key){
                        writer.write("'");
                    }else{
                        System.out.print("'");  //inicio de um paragrafo
                    }
                    if(((Questao)comps[i]).isTaNegri()){
                        if(key){
                            writer.write("{");
                        }else{
                            System.out.print("{");
                        }
                    }
                    for (int j = 0; j < origem.length(); j++) {
                        if(origem.codePointAt(j) != 10){
                            if(key){
                                writer.write(origem.charAt(j));
                            }else{
                                System.out.print(origem.charAt(j));
                            }
                        }else{
                            if(((Questao)comps[i]).isTaNegri()){
                                if(key){
                                    writer.write("}");
                                }else{
                                    System.out.print("}");
                                }
                            }
                            if(key){
                                writer.write("'");
                                writer.newLine();
                                writer.write("'");
                            }else{
                                System.out.print("'\n'");   //final de um paragrafo e inicio de outro
                            }
                            if(((Questao)comps[i]).isTaNegri()){
                                if(key){
                                    writer.write("{");
                                }else{
                                    System.out.print("{");
                                }
                            }
                        }
                    }
                    if(((Questao)comps[i]).isTaNegri()){
                        if(key){
                            writer.write("}");
                        }else{
                            System.out.print("}");
                        }
                    }
                    if(key){
                        writer.write("'");
                        writer.newLine();
                    }else{
                        System.out.println("'");    //final do ultimo paragrafo
                    }
                }else if (comps[i] instanceof Alternativa) {
                    String origem = ((Alternativa)comps[i]).getText();
                    if(key){
                        writer.write("'");
                    }else{
                        System.out.print("'");  //inicio de um paragrafo
                    }
                    if(((Alternativa)comps[i]).isTaNegri()){
                        if(key){
                            writer.write("{");
                        }else{
                            System.out.print("{");
                        }
                    }
                    for (int j = 0; j < origem.length(); j++) {
                        if(origem.codePointAt(j) != 10){
                            if(key){
                                writer.write(origem.charAt(j));
                            }else{
                                System.out.print(origem.charAt(j));
                            }
                        }else{
                            if(((Alternativa)comps[i]).isTaNegri()){
                                if(key){
                                    writer.write("}");
                                }else{
                                    System.out.print("}");
                                }
                            }
                            if(key){
                                writer.write("'");
                                writer.newLine();
                                writer.write("'");
                            }else{
                                System.out.print("'\n'");   //final de um paragrafo e inicio de outro
                            }
                            if(((Alternativa)comps[i]).isTaNegri()){
                                if(key){
                                    writer.write("{");
                                }else{
                                    System.out.print("{");
                                }
                            }
                        }
                    }
                    if(((Alternativa)comps[i]).isTaNegri()){
                        if(key){
                            writer.write("}");
                        }else{
                            System.out.print("}");
                        }
                    }
                    if(key){
                        writer.write("'");
                        writer.newLine();
                    }else{
                        System.out.println("'");    //final do ultimo paragrafo
                    }
                }else if (comps[i] instanceof ImagEsq) {
                    String origem = ((ImagEsq)comps[i]).getImp().getFile();
                    origem = ((ImagEsq)comps[i]).getW()+"W"+((ImagEsq)comps[i]).getH()+"H"+origem;
                    if(((ImagEsq)comps[i]).getImp().getAlinhamento().equals("ESQUERDA")){
                        origem = "--"+origem;
                    }else if(((ImagEsq)comps[i]).getImp().getAlinhamento().equals("DIREITA")){
                        origem = "---"+origem;
                    }
                    if(key){
                        writer.write(origem);
                        writer.newLine();
                    }else{
                        System.out.println(origem);
                    }
                }
                /*if(key){
                    writer.write("-");
                    writer.newLine();
                }else{
                    System.out.println("-");    //Representa uma tecla enter entre um componente da folha e outro.
                }*/
            }
        } catch (IOException ex) {
            textCfg.setForeground(Color.red);
            textCfg.setText("pasta \"Necessario\"\nNão encontrada!");
        }
    }
    
    public ImageIcon aumentaImagem(ImageIcon arquivo, int alt, int larg){
        ImageIcon thumbnail = thumbnail = new ImageIcon(arquivo.getImage().getScaledInstance(larg, alt, Image.SCALE_SMOOTH));
        return thumbnail;
    }
    public void atualizar(){
        for (int i = 0; i < papeis.size(); i++) {
            papeis.get(i).setVisible(false);
            papeis.get(i).setVisible(true);
        }
    }
    public void atualizarToolTip(){
        String letra = "  ";
        for (int i = 0; i < conteudos.size(); i++) {
            if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(1)).isSelected()){
                letra = ";  Resposta: (a)";   //letra a
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(2)).isSelected()){
                letra = ";  Resposta: (b)";   //letra b
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(3)).isSelected()){
                letra = ";  Resposta: (c)";   //letra c
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(4)).isSelected()){
                letra = ";  Resposta: (d)";   //letra d
            }else if(((JRadioButton)((Box)configurar3.getComponent(i)).getComponent(5)).isSelected()){
                letra = ";  Resposta: (e)";   //letra e
            }else{
                letra = "";   // sem alternativas marcadas
            }
            conteudos.get(i).setToolTipText("Questão: "+conteudos.get(i).getQuestN()+letra);
        }
    }
    
    public void ajustaSegColuna(int numDaPag){if(areas.get(areaDaPagAtual(numDaPag - 1) + 1).getComponents().length != 0){ //Se a segunda coluna é diferente de zero
            int quantCompSegCol = areas.get(areaDaPagAtual(numDaPag - 1) + 1).getComponents().length;
            while(quantCompSegCol != 0){
                Component comps[] = areas.get(areaDaPagAtual(numDaPag - 1)).getComponents();
                int comprimento = 0;    //soma da altura de todos os componentes de uma lauda
                for (int j = 0; j < comps.length; j++) {
                    if (comps[j] instanceof Questao) {   // verifica se é uma Questão
                        comprimento += ((Questao)comps[j]).getHeight();
                        comprimento += 5;   //Soma o espaçamento entre questões
                    }else if (comps[j] instanceof ImagEsq) {
                        comprimento += ((ImagEsq)comps[j]).getImp().getHeight();
                        comprimento += 5;   //Soma o espaçamento entre questões
                    }else if (comps[j] instanceof Alternativa) {
                        comprimento += ((Alternativa)comps[j]).getHeight();
                        comprimento += 5;   //Soma o espaçamento entre questões
                    }
                }
                Component comp2 = areas.get(areaDaPagAtual(numDaPag - 1) + 1).getComponents()[0];   //Pega primeiro componente da segunda coluna
                int comprimento2 = 0;
                if (comp2 instanceof Questao) {   // verifica se é uma Questão
                    comprimento2 += ((Questao)comp2).getHeight();
                    comprimento2 += 5;   //Soma o espaçamento entre questões
                }else if (comp2 instanceof ImagEsq) {
                    comprimento2 += ((ImagEsq)comp2).getImp().getHeight();
                    comprimento2 += 5;   //Soma o espaçamento entre questões
                }else if (comp2 instanceof Alternativa) {
                    comprimento2 += ((Alternativa)comp2).getHeight();
                    comprimento2 += 5;   //Soma o espaçamento entre questões
                }
                if(comprimento + comprimento2 <= 720){
                    areas.get(areaDaPagAtual(numDaPag - 1)).add(comp2); //Adiciona na coluna um
                    areas.get(areaDaPagAtual(numDaPag - 1) + 1).remove(comp2);  //Remove da coluna dois
                }
                quantCompSegCol--;
            }
        }
    }
    
    public void apagaTudoDaQuest(int numDaQuest){   //apaga tudo o que tiver entre a questão do parametro e a proxima ou o final dos componentes
        boolean excluir = false,    //chave que é liberada após encontrar a posição da questão que está sendo excluida
                continuaOloop = true;   //chave para continuar dentro do loop maior
        for (int i = 0; i < areas.size(); i++) {
            if(continuaOloop){
                Component comps[] = areas.get(i).getComponents();
                for (int j = 0; j < comps.length; j++) {
                    if(!excluir){
                        if (comps[j] instanceof Questao) {  //Vai procurar a questão que foi fornecida como parametro
                            if(conteudos.get(((Questao)comps[j]).getQuestN() - 1).getQuestN() == conteudos.get(numDaQuest).getQuestN()){    //se a questão encontrada na varredura for a msm da solicitada nos parametros
                                excluir = true; //Para a procura da questão para começar as exclusões
                            }
                        }
                    }else{  //Começa as exclusões
                        if (comps[j] instanceof Questao) {
                            continuaOloop = false;  //para de vez o loop maior
                            break;  //Se achar uma proxima questão interrompe o loop sem apagar a proxima questão
                        }else if (comps[j] instanceof ImagEsq) {
                            brancos.remove(comps[j]);
                            areas.get(i).remove(comps[j]);  //Enquanto não achar questão vai excluindo tudo
                        }else if (comps[j] instanceof Alternativa) {
                            alter.remove(comps[j]);
                            areas.get(i).remove(comps[j]);  //Enquanto não achar questão vai excluindo tudo
                        }
                    }
                }
            }else{
                for (int j = 0; j < papeis.size(); j++) {
                    if(papeis.get(j).getColunas() == 2){
                        ajustaSegColuna(j + 1);
                    }
                }
                atualizar();
                break;
            }
        }
    }
    
    public void coloreTudoDaQuest(int numDaQuest, boolean colore){   //apaga tudo o que tiver entre a questão do parametro e a proxima ou o final dos componentes
        boolean excluir = false,    //chave que é liberada após encontrar a posição da questão que está sendo colorica
                continuaOloop = true;   //chave para continuar dentro do loop maior
        if(colore){
            conteudos.get(numDaQuest).setBackground(Color.yellow);
        }else{
            conteudos.get(numDaQuest).setBackground(Color.white);
        }
        for (int i = 0; i < areas.size(); i++) {
            if(continuaOloop){
                Component comps[] = areas.get(i).getComponents();
                for (int j = 0; j < comps.length; j++) {
                    if(!excluir){
                        if (comps[j] instanceof Questao) {  //Vai procurar a questão que foi fornecida como parametro
                            if(conteudos.get(((Questao)comps[j]).getQuestN() - 1).getQuestN() == conteudos.get(numDaQuest).getQuestN()){    //se a questão encontrada na varredura for a msm da solicitada nos parametros
                                excluir = true; //Para a procura da questão para começar as exclusões
                            }
                        }
                    }else{  //Começa as exclusões
                        if(colore){
                            if (comps[j] instanceof Questao) {
                                continuaOloop = false;  //para de vez o loop maior
                                break;  //Se achar uma proxima questão interrompe o loop sem apagar a proxima questão
                            }else if (comps[j] instanceof ImagEsq) {
                                if(!((ImagEsq)comps[j]).getImp().isClicado()){
                                    ((ImagEsq)comps[j]).getImp().setBorder(new LineBorder(Color.yellow,2));
                                }
                            }else if (comps[j] instanceof Alternativa) {
                                ((Alternativa)comps[j]).setBackground(Color.yellow);
                            }
                        }else{
                            if (comps[j] instanceof Questao) {
                                continuaOloop = false;  //para de vez o loop maior
                                break;  //Se achar uma proxima questão interrompe o loop sem apagar a proxima questão
                            }else if (comps[j] instanceof ImagEsq) {
                                if(!((ImagEsq)comps[j]).getImp().isClicado()){
                                    ((ImagEsq)comps[j]).getImp().setBorder(null);
                                }
                            }else if (comps[j] instanceof Alternativa) {
                                ((Alternativa)comps[j]).setBackground(Color.white);
                            }
                        }
                    }
                }
            }else{
                break;
            }
        }
    }
    
    public void apagarDaArea(Component esse){
        for (int i = 0; i < areas.size(); i++) {
            Component comps[] = areas.get(i).getComponents();
            for (int j = 0; j < comps.length; j++) {
                if((esse instanceof Questao)&&(comps[j] instanceof Questao)){
                    if(((Questao)esse).getQuestN() == ((Questao)comps[j]).getQuestN()){
                        apagaTudoDaQuest(((Questao)comps[j]).getQuestN() - 1);    //Apaga tudo oq vier depois da questão até uma proxima questão
                        areas.get(i).remove((Questao)comps[j]);
                        break;
                    }
                }else if((esse instanceof ImagEsq)&&(comps[j] instanceof ImagEsq)){
                    if(((ImagEsq)esse).getNum() == ((ImagEsq)comps[j]).getNum()){
                        areas.get(i).remove((ImagEsq)comps[j]);
                        break;
                    }
                }else if((esse instanceof Alternativa)&&(comps[j] instanceof Alternativa)){
                    if(((Alternativa)esse).getIdentidade() == ((Alternativa)comps[j]).getIdentidade()){
                        areas.get(i).remove((Alternativa)comps[j]);
                        break;
                    }
                }
            }
        }
        atualizar();
    }
    
    private int x=0,y=0,antX = 0,antY = 0;;
    public void arrastar(){
        imagCfg.addMouseMotionListener(new MouseAdapter(){
            @Override
            public void mouseDragged(MouseEvent e) {
                int newX, newY;
                imagCfg.setIcon(volatil);
                newX = imagCfg.getX()- (x - e.getX());
                newY = imagCfg.getY() - (y - e.getY());
                
                e.getX();
                e.getY();
                int maxX = janela.getWidth()-imagCfg.getWidth();
                int maxY= janela.getHeight()-imagCfg.getHeight();
                if (newX < 0)    newX = 0;
                if (newX > maxX) newX = maxX;
                if (newY < 0)    newY = 0;
                if (newY > maxY) newY = maxY;
                
                imagCfg.setLocation(newX, newY);
                
            }
        });
    
    }
    private boolean permite = false, mexer = false;
    private Component exclui = null;
    public void mouse(){
        ml = new MouseListener(){
            public void mouseEntered(java.awt.event.MouseEvent evt){
                Pagina label = (Pagina) evt.getSource();
                for (int i = 0; i < numPag; i++) {
                    if(label.getPagN() <= papeis.get(i).getPagN()){
                        pagTemp = label.getPagN();
                        for (int j = 0; j < papeis.size(); j++) {
                            if(!papeis.get(j).isClicada()){
                                papeis.get(j).setBorder(null);
                            }
                        }
                        if (!label.isClicada()) {
                            label.setBorder(new LineBorder(Color.orange,2));
                        }
                        label.getNumRodape().setVisible(true);
                        label.getImageApagar().setVisible(true);
                        label.getLabelNumCol().setVisible(true);
                        break;
                    }
                }
                
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {}
            public void mousePressed(java.awt.event.MouseEvent evt) {}
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                Pagina label = (Pagina)evt.getSource();
                pagClick = label.getPagN();
                
                for (int i = 0; i < papeis.size(); i++) {
                    papeis.get(i).setClicada(false);
                    papeis.get(i).setBorder(null);
                }
                mostrarPag.setText("Página: ("+pagClick+" de "+papeis.size()+")");
                label.setClicada(true);
                label.setBorder(new LineBorder(Color.red,2));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Pagina label = (Pagina)evt.getSource();
                label.getImageApagar().setVisible(false);
                label.getNumRodape().setVisible(false);
                label.getLabelNumCol().setVisible(false);
                for (int i = 0; i < papeis.size(); i++) {
                    if(!label.isClicada()){
                        label.setBorder(null);
                    }
                }
            }
        };
        
        ml2 = new MouseListener(){
            public void mouseEntered(java.awt.event.MouseEvent evt){
                JLabel label = (JLabel)evt.getSource();
                File origem = new File(label.getIcon().toString());
                //System.out.println("origem: "+origem.getName().replaceFirst("[.][^.]+$", "").toString());
                if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("Apagar")){
                    label.setVisible(true);
                    label.setIcon(iconeApagarAC);
                }else if(label.getIcon() == iconeApagarAC){
                    label.setVisible(true);
                }else if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("ApagarPeq")){
                    label.setVisible(true);
                    label.setIcon(iconeApagarPeAC);
                }else if(label.getIcon() == iconeApagarPeAC){
                    label.setVisible(true);
                }else if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("SetaEsq")){
                    label.setVisible(true);
                    label.setIcon(iconeSetaEsqAC);
                }else if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("SetaCen")){
                    label.setVisible(true);
                    label.setIcon(iconeSetaCenAC);
                }else if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("SetaDir")){
                    label.setVisible(true);
                    label.setIcon(iconeSetaDirAC);
                }else if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("negrito")){
                    label.setVisible(true);
                    label.setIcon(iconeNegritoAC);
                }else if(label.getIcon() == iconeNegritoAC){
                    label.setVisible(true);
                }
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {}
            public void mousePressed(java.awt.event.MouseEvent evt) {}
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                JLabel label = (JLabel)evt.getSource();
                if(label.getIcon() == iconeApagarAC){
                    apagarPagina();
                }else if(label.getIcon() == iconeSetaDirAC){
                    mexer = true;
                    for (int i = 0; i < brancos.size(); i++) {
                        if (brancos.get(i).getImp().getNumero() == temp.getNumero()) {
                            brancos.get(i).getImp().alinhar("DIREITA",papeis.get(pagTemp - 1).getColunas());
                            break;
                        }
                    }
                }else if(label.getIcon() == iconeSetaCenAC){
                    mexer = true;
                    for (int i = 0; i < brancos.size(); i++) {
                        if (brancos.get(i).getImp().getNumero() == temp.getNumero()) {
                            brancos.get(i).getImp().alinhar("CENTRO",papeis.get(pagTemp - 1).getColunas());
                            break;
                        }
                    }
                }else if(label.getIcon() == iconeSetaEsqAC){
                    mexer = true;
                    for (int i = 0; i < brancos.size(); i++) {
                        if (brancos.get(i).getImp().getNumero() == temp.getNumero()) {
                            brancos.get(i).getImp().alinhar("ESQUERDA",papeis.get(pagTemp - 1).getColunas());
                            break;
                        }
                    }
                }else if(label.getIcon() == iconeApagarPeAC){
                    if (exclui instanceof Questao) {
                        for (int i = 0; i < conteudos.size(); i++) {
                            if (conteudos.get(i).getQuestN() == ((Questao)exclui).getQuestN()) {
                                apagarDaArea(conteudos.get(i));
                                conteudos.remove(i);
                                if(papeis.get(pagTemp - 1).getColunas() == 2){
                                    ajustaSegColuna(pagTemp);
                                }
                                numQuest--;
                                ajustaNumQuest();
                                ajustaMaiorMenor();
                                break;
                            }
                        }
                    }else if (exclui instanceof Imagem) {
                        for (int i = 0; i < brancos.size(); i++) {
                            if (brancos.get(i).getImp().getNumero() == ((Imagem)exclui).getNumero()) {
                                apagarDaArea(brancos.get(i));
                                if(papeis.get(pagTemp - 1).getColunas() == 2){
                                    ajustaSegColuna(pagTemp);
                                }
                                brancos.remove(i);
                                break;
                            }
                        }
                    }else if (exclui instanceof Alternativa) {
                        for (int i = 0; i < alter.size(); i++) {
                            if (alter.get(i).getIdentidade() == ((Alternativa)exclui).getIdentidade()) {
                                apagarDaArea(alter.get(i));
                                if(papeis.get(pagTemp - 1).getColunas() == 2){
                                    ajustaSegColuna(pagTemp);
                                }
                                alter.remove(i);
                                break;
                            }
                        }
                    }
                }else if(label.getIcon() == iconeNegritoAC){
                    if (exclui instanceof Questao) {
                        if(conteudos.get(questTemp - 1).isTaNegri()){
                            conteudos.get(questTemp - 1).setFont(new Font("TIMES_ROMAN", Font.PLAIN, 11));
                            conteudos.get(questTemp - 1).setTaNegri(false);
                        }else{
                            conteudos.get(questTemp - 1).setFont(new Font("TIMES_ROMAN", Font.BOLD, 11));
                            conteudos.get(questTemp - 1).setTaNegri(true);
                        }
                    }else if (exclui instanceof Alternativa) {
                        for (int i = 0; i < alter.size(); i++) {
                            if (alter.get(i).getIdentidade() == ((Alternativa)exclui).getIdentidade()) {
                                if(alter.get(i).isTaNegri()){
                                    alter.get(i).setFont(new Font("TIMES_ROMAN", Font.PLAIN, 11));
                                    alter.get(i).setTaNegri(false);
                                }else{
                                    alter.get(i).setFont(new Font("TIMES_ROMAN", Font.BOLD, 11));
                                    alter.get(i).setTaNegri(true);
                                }
                                break;
                            }
                        }
                    }
                }
                 mostrarPag.setText("Total de páginas: "+papeis.size());
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                JLabel label = (JLabel)evt.getSource();
                if(label.getIcon() == iconeApagarAC){
                    label.setVisible(true);
                    label.setIcon(iconeApagar);
                }else if(label.getIcon() == iconeApagarPeAC){
                    label.setVisible(true);
                    label.setIcon(iconeApagarPe);
                }else if(label.getIcon() == iconeSetaEsqAC){
                    if(!mexer){
                        label.setVisible(true);
                    }else{
                        label.setVisible(false);
                        mexer = false;
                    }
                    label.setIcon(iconeSetaEsq);
                }else if(label.getIcon() == iconeSetaCenAC){
                    if(!mexer){
                        label.setVisible(true);
                    }else{
                        label.setVisible(false);
                        mexer = false;
                    }
                    label.setIcon(iconeSetaCen);
                }else if(label.getIcon() == iconeSetaDirAC){
                    if(!mexer){
                        label.setVisible(true);
                    }else{
                        label.setVisible(false);
                        mexer = false;
                    }
                    label.setIcon(iconeSetaDir);
                }else if(label.getIcon() == iconeNegritoAC){
                    label.setVisible(true);
                    label.setIcon(iconeNegrito);
                }
            }
        };
        
        ml3 = new MouseListener(){
            public void mouseEntered(java.awt.event.MouseEvent evt){
                Questao label = (Questao)evt.getSource();
                exclui = label;
                atualizarToolTip();
                label.setBackground(Color.white);
                for (int i = 0; i < numQuest; i++) {
                    if(label.getQuestN() <= conteudos.get(i).getQuestN()){
                        questTemp = label.getQuestN();
                        label.setBorder(new LineBorder(Color.LIGHT_GRAY));
                        label.getCaixaDeNum().setVisible(true);
                        label.getApag().setVisible(true);
                        label.getNegri().setVisible(true);
                        break;
                    }
                }
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {}
            public void mousePressed(java.awt.event.MouseEvent evt) {}
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                Questao label = (Questao)evt.getSource();
                for (int i = 0; i < papeis.size(); i++) {
                    papeis.get(i).setBorder(null);
                    papeis.get(i).setClicada(false);
                    if(papeis.get(i).getPagN() == papeis.get(pagTemp - 1).getPagN() ){
                        papeis.get(i).setClicada(true);
                        papeis.get(i).setBorder(new LineBorder(Color.red,2));
                        pagClick = pagTemp;
                    }
                }
                for (int i = 0; i < conteudos.size(); i++) {
                    coloreTudoDaQuest(i,false); //Descolore todos os componentes
                    if(conteudos.get(i).getQuestN() == label.getQuestN() ){
                        coloreTudoDaQuest(i,true);  //Colore só os componentes da questão clicada
                        questClick = i + 1;
                    }
                }
                mostrarPag.setText("Total de páginas: "+papeis.size());
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Questao label = (Questao)evt.getSource();
                for (int i = 0; i < numQuest; i++) {
                    if(label.getQuestN() <= conteudos.get(i).getQuestN()){
                        label.getApag().setVisible(false);
                        label.getNegri().setVisible(false);
                        label.getCaixaDeNum().setVisible(false);
                        label.setBorder(null);
                        break;
                    }
                }
            }
        };
        
        ml4 = new MouseListener(){
            public void mouseEntered(java.awt.event.MouseEvent evt){
                Imagem label = (Imagem)evt.getSource();
                label.getApag().setVisible(true);
                tam.setFocusable(false);
                File origem = new File(label.getIcon().toString());
                if(origem.getName().replaceFirst("[.][^.]+$", "").toString().equals("ImagemSconteudo")){
                    label.setIcon(iconeSconteudoAC);
                }
                if(label != imagCfg){
                    for (int i = 0; i < brancos.size(); i++) {
                        label.setEntrou(false);
                    }
                    label.setEntrou(true);
                    for (int i = 0; i < brancos.size(); i++) {
                        if(brancos.get(i).getImp().getNumero() == label.getNumero()){
                            temp = label;
                            permite = true;
                            break;
                        }
                    }
                }else{
                    imagCfg.setBorder(new LineBorder(Color.LIGHT_GRAY,2));
                }
                label.getApag().setVisible(true);
                if (label.isVaiTer()) {
                    label.getEs().setVisible(true);
                    label.getCe().setVisible(true);
                    label.getDi().setVisible(true);
                }
                
                int num = 0;
                for (int i = 0; i < brancos.size(); i++) {
                    if(brancos.get(i).getImp().getNumero() == label.getNumero()){
                        num = i;
                        break;
                    }
                }
                if(label != imagCfg){
                    exclui = label;
                    if ((brancos.get(num).getImp().getNumero() == label.getNumero()) && !label.isClicado()) {
                        boolean temClicada = false;
                        label.setBorder(new LineBorder(Color.BLUE,3));
                        
                    }
                }
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {}
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Imagem label = (Imagem)evt.getSource();
                if(trava == 0){
                    antX = label.getX();
                    antY = label.getY();
                    trava++;
                }
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                Imagem label = (Imagem)evt.getSource();
                trava = 0;
                boolean entra = true;
                if(label == imagCfg){
                    entra = false;
                    int num2 = 0;
                    boolean ch = false, ok = false;
                    for (int i = 0; i < brancos.size(); i++) {
                        if (brancos.get(i).getImp().isClicado() && brancos.get(i).getImp().isEntrou()) {
                            ok = true;
                        }
                    }
                    if (ok) {
                        for (int i = 0; i < brancos.size(); i++) {
                            if (brancos.get(i).getImp().isClicado() && (brancos.get(i).getImp().getNumero() == temp.getNumero())) {
                                num2 = i;
                                ch = true;
                                break;
                            }
                        }
                        if (ch) {
                            brancos.get(num2).getImp().setIcon(volatil);
                            brancos.get(num2).getImp().setSize(volatil.getIconWidth(), volatil.getIconHeight());
                            if(papeis.get(pagClick - 1).getColunas() == 1){
                                brancos.get(num2).setIcon(aumentaImagem(iconeBranco, volatil.getIconHeight(),525));    //Aumenta o tamanho vertical do fundo branco
                            }else if(papeis.get(pagTemp - 1).getColunas() == 2){
                                brancos.get(num2).setIcon(aumentaImagem(iconeBranco, volatil.getIconHeight(),240));    //Aumenta o tamanho vertical do fundo branco
                            }
                            brancos.get(num2).setW(volatil.getIconWidth());
                            brancos.get(num2).setH(volatil.getIconHeight());
                            
                            if (brancos.get(num2).getImp().getAlinhamento().equals("ESQUERDA")) {
                                brancos.get(num2).getImp().setLocation(0, 0);
                            }else if (brancos.get(num2).getImp().getAlinhamento().equals("CENTRO")) {
                                if(papeis.get(pagClick - 1).getColunas() == 1){
                                    brancos.get(num2).getImp().setLocation((525 - brancos.get(num2).getImp().getWidth())/2, 0);
                                }else if(papeis.get(pagClick - 1).getColunas() == 2){
                                    brancos.get(num2).getImp().setLocation((240 - brancos.get(num2).getImp().getWidth())/2, 0);
                                }
                            }else if (brancos.get(num2).getImp().getAlinhamento().equals("DIREITA")) {
                                if(papeis.get(pagClick - 1).getColunas() == 1){
                                    brancos.get(num2).getImp().setLocation(525 - brancos.get(num2).getW() - 5, 0);
                                }else if(papeis.get(pagClick - 1).getColunas() == 2){
                                    brancos.get(num2).getImp().setLocation(240 - brancos.get(num2).getW(), 0);
                                }
                            }
                            brancos.get(num2).getImp().getApag().setBounds(volatil.getIconWidth() - iconeApagarPeq.getIconWidth() - 7, 10, iconeApagarPeq.getIconWidth(), iconeApagarPeq.getIconHeight());
                            brancos.get(num2).getImp().getEs().setLocation(brancos.get(num2).getImp().getIcon().getIconWidth() - iconeSetaEsq.getIconWidth() - 52,10);
                            brancos.get(num2).getImp().getCe().setLocation(brancos.get(num2).getImp().getIcon().getIconWidth() - iconeSetaCen.getIconWidth() - 37,10);
                            brancos.get(num2).getImp().getDi().setLocation(brancos.get(num2).getImp().getIcon().getIconWidth() - iconeSetaDir.getIconWidth() - 21,10);
                            
                            brancos.get(num2).getImp().setFile(fileTemp.replaceAll("[\\\\]","\\\\"+"\\\\"));
                            
                            brancos.add(num2, brancos.get(num2));
                        }
                    }
                    imagCfg.setLocation(antX, antY);
                }
                if (entra) {
                    int num = 0;
                    boolean pode = false;
                    for (int i = 0; i < brancos.size(); i++) {
                        if (brancos.get(i).getImp().getNumero() == label.getNumero()) {
                            pode = true;
                            num = i;
                        }
                        brancos.get(i).getImp().setClicado(false);
                        brancos.get(i).getImp().setBorder(null);
                    }
                    if(pode){
                        brancos.get(num).getImp().setClicado(true);
                    }
                    for (int i = 0; i < papeis.size(); i++) {
                        papeis.get(i).setBorder(null);
                        papeis.get(i).setClicada(false);
                    }
                    papeis.get(pagTemp - 1).setClicada(true);
                    papeis.get(pagTemp - 1).setBorder(new LineBorder(Color.red,3));
                    pagClick = pagTemp;
                    brancos.get(num).getImp().setBorder(new LineBorder(Color.red,3));
                }
                
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Imagem label = (Imagem)evt.getSource();
                tam.setFocusable(true);
                label.getApag().setVisible(false);
                if(label.isVaiTer()){
                    label.getEs().setVisible(false);
                    label.getCe().setVisible(false);
                    label.getDi().setVisible(false);
                }
                if(label.getIcon() == iconeSconteudoAC ){
                    label.setIcon(iconeSconteudo);
                    
                }
                int num = 0;
                for (int i = 0; i < brancos.size(); i++) {
                    if(brancos.get(i).getImp().getNumero() == label.getNumero()){
                        num = i;
                        permite = false;
                        break;
                    }
                }
                if (label != imagCfg) {
                    if (!label.isClicado()) {
                        brancos.get(num).getImp().setBorder(null);
                    }
                    for (int i = 0; i < brancos.size(); i++) {
                        if (!brancos.get(i).getImp().isClicado()) {
                            brancos.get(i).getImp().setClicado(false);
                        }
                    }
                }else{
                    imagCfg.setBorder(null);
                }
                
            }
        };
        
        ml5 = new MouseListener(){
            public void mouseEntered(java.awt.event.MouseEvent evt){
                Alternativa label = (Alternativa)evt.getSource();
                exclui = label;
                label.setBackground(Color.white);
                label.setBorder(new LineBorder(Color.LIGHT_GRAY));
                label.getApag().setVisible(true);
                label.getNegri().setVisible(true);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {}
            public void mousePressed(java.awt.event.MouseEvent evt) {}
            public void mouseReleased(java.awt.event.MouseEvent evt) {}
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Alternativa label = (Alternativa)evt.getSource();
                label.getApag().setVisible(false);
                label.getNegri().setVisible(false);
                label.setBorder(null);
            }
        };
    }
        
    public int areaParaPag(int numArea){
        int somaPag = 0;
        for (int i = 0; i <= numArea; i++) {
            if(papeis.get(somaPag).getColunas() == 2){
                i++;
            }
            somaPag++;
        }
        return somaPag; //Retorna o valor da página que contem a area mandada
    }
    
    public class QuebraDpag extends Thread{
        public void run(){
            while(true){
                try{sleep(10);}catch(Exception erro){}
                if(papeis.size() > 0 && conteudos.size() > 0){  //Se tiver ao menos uma pagina e ao menos uma questão na pagina
                    for (int i = 0; i < areas.size(); i++) {
                        Component comps[] = (areas.get(i)).getComponents(); // retorna todos os componentes do JPanel
                        int comprimento = 0;    //soma da altura de todos os componentes de uma lauda
                        for (int j = 0; j < comps.length; j++) {
                            if (comps[j] instanceof Questao) {   // verifica se é uma Questão
                                comprimento += ((Questao)comps[j]).getHeight();
                                comprimento += 5;   //Soma o espaçamento entre questões
                            }else if (comps[j] instanceof ImagEsq) {
                                comprimento += ((ImagEsq)comps[j]).getImp().getHeight();
                                comprimento += 5;   //Soma o espaçamento entre questões
                            }else if (comps[j] instanceof Alternativa) {
                                comprimento += ((Alternativa)comps[j]).getHeight();
                                comprimento += 5;   //Soma o espaçamento entre questões
                            }
                        }
                        if(comprimento > 720){      //Se a soma da altura de todos os componentes é maior que o tamanho exigido pela lauda
                            if(areas.size() == 1 || areas.size() - 1 == i){ //Se for a primeira página sendo criada ou se estiver na ultima página
                                numPag++;
                                if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) { //Se o lugar que está sendo removido é de uma coluna
                                    criarPag(); //adicione uma página de uma coluna
                                }else if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) { //Se o lugar que está sendo removido é de duas colunas
                                    criarPagDuasColu(); //adicione uma página de duas coluna
                                }
                                if(pagClick > 0){
                                    mostrarPag.setText("Página: ("+pagClick+" de "+papeis.size()+")");
                                }else{
                                    mostrarPag.setText("Total de páginas: "+papeis.size());
                                }
                                if (comps[comps.length - 1] instanceof Questao) {   // verifica se é uma Questão
                                    int numQuest = 0;
                                    boolean exclua = false;
                                    for (int j = 0; j < conteudos.size(); j++) {
                                        if (conteudos.get(j).getQuestN() == ((Questao)comps[comps.length - 1]).getQuestN()) {
                                            numQuest = j;
                                            exclua = true;
                                            break;
                                        }
                                    }
                                    if(exclua){
                                        if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) { //Se o lugar que está sendo removido é de uma coluna
                                            areas.get(areas.size() - 1).add(conteudos.get(numQuest)); //adicione à ultima area
                                        }else if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) { //Se o lugar que está sendo removido é de duas colunas
                                            areas.get(areas.size() - 2).add(conteudos.get(numQuest)); //adicione à penultima area
                                        }
                                        areas.get(i).remove(conteudos.get(numQuest));   //remove o componente do lugar que costumava ficar
                                    }
                                    ajustaMaiorMenor();
                                }else if (comps[comps.length - 1] instanceof ImagEsq) {
                                    int numQuest = 0;
                                    boolean exclua = false;
                                    for (int j = 0; j < brancos.size(); j++) {
                                        if (brancos.get(j).getNum() == ((ImagEsq)comps[comps.length - 1]).getNum()) {
                                            numQuest = j;
                                            exclua = true;
                                            break;
                                        }
                                    }
                                    if(exclua){
                                        if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) { //Se o lugar que está sendo removido é de uma coluna
                                            areas.get(areas.size() - 1).add(brancos.get(numQuest)); //adicione à ultima area
                                        }else if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) { //Se o lugar que está sendo removido é de duas colunas
                                            areas.get(areas.size() - 2).add(brancos.get(numQuest)); //adicione à penultima area
                                        }
                                        areas.get(i).remove(brancos.get(numQuest));
                                    }
                                }else if (comps[comps.length - 1] instanceof Alternativa) {
                                    int numQuest = 0;
                                    boolean exclua = false;
                                    for (int j = 0; j < alter.size(); j++) {
                                        if (alter.get(j).getIdentidade() == ((Alternativa)comps[comps.length - 1]).getIdentidade()) {
                                            numQuest = j;
                                            exclua = true;
                                            break;
                                        }
                                    }
                                    if(exclua){
                                        if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) { //Se o lugar que está sendo removido é de uma coluna
                                            areas.get(areas.size() - 1).add(alter.get(numQuest)); //adicione à ultima area
                                        }else if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) { //Se o lugar que está sendo removido é de duas colunas
                                            areas.get(areas.size() - 2).add(alter.get(numQuest)); //adicione à penultima area
                                        }
                                        areas.get(i).remove(alter.get(numQuest));
                                    }
                                }
                            }else{
                                if (comps[comps.length - 1] instanceof Questao) {   // verifica se é uma Questão
                                    int numQuest = 0;
                                    boolean exclua = false;
                                    for (int j = 0; j < conteudos.size(); j++) {
                                        if (conteudos.get(j).getQuestN() == ((Questao)comps[comps.length - 1]).getQuestN()) {
                                            numQuest = j;
                                            exclua = true;
                                            break;
                                        }
                                    }
                                    if(exclua){
                                        //----------Altera dimensões do fundo branco da imagem-------------
                                        if (papeis.get(areaParaPag(i + 1) - 1).getColunas() == 1) { //Se o lugar que está sendo adicionado é de uma coluna
                                            if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) {   //Se o lugar que está sendo removido é de duas colunas
                                                conteudos.get(numQuest).setSize(525, 40);
                                            }
                                        }else if (papeis.get(areaParaPag(i + 1) - 1).getColunas() == 2) {   //Se o lugar que está sendo adicionado é de duas colunas
                                            if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) {   //Se o lugar que está sendo removido é de uma coluna
                                                conteudos.get(numQuest).setSize(245, 40);
                                            }
                                        }
                                        conteudos.get(numQuest).getApag().setBounds(conteudos.get(numQuest).getWidth() - iconeApagarPeq.getIconWidth() - 8,3,iconeApagarPeq.getIconWidth(), iconeApagarPeq.getIconHeight());
                                        conteudos.get(numQuest).getApag().setBounds(conteudos.get(numQuest).getWidth() - iconeNegrito.getIconWidth() - 23,2,iconeNegrito.getIconWidth(), iconeNegrito.getIconHeight());
                                        areas.get(i + 1).add(conteudos.get(numQuest),0);
                                        areas.get(i).remove(conteudos.get(numQuest));
                                    }
                                    ajustaMaiorMenor();
                                }else if (comps[comps.length - 1] instanceof ImagEsq) {
                                    int numQuest = 0;
                                    boolean exclua = false;
                                    for (int j = 0; j < brancos.size(); j++) {
                                        if (brancos.get(j).getNum() == ((ImagEsq)comps[comps.length - 1]).getNum()) {
                                            numQuest = j;
                                            exclua = true;
                                            break;
                                        }
                                    }
                                    if(exclua){
                                        //----------Altera dimensões do fundo branco da imagem-------------
                                        if (papeis.get(areaParaPag(i + 1) - 1).getColunas() == 1) { //Se o lugar que está sendo adicionado é de uma coluna
                                            if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) {   //Se o lugar que está sendo removido é de duas colunas
                                                brancos.get(numQuest).setIcon(aumentaImagem(iconeBranco, iconeSconteudo.getIconHeight(),525));    //Aumenta o tamanho horizontal do fundo branco
                                                if(brancos.get(numQuest).getImp().getAlinhamento().equals("CENTRO")){   //Ajusta alinhamento ao centro
                                                    brancos.get(numQuest).getImp().setLocation((525 - brancos.get(numQuest).getImp().getIcon().getIconWidth())/2, 0);
                                                }else if(brancos.get(numQuest).getImp().getAlinhamento().equals("DIREITA")){    //Ajusta alinhamento à direita
                                                    brancos.get(numQuest).getImp().setLocation(525 - brancos.get(numQuest).getImp().getIcon().getIconWidth() - 5, 0);
                                                }
                                            }
                                        }else if (papeis.get(areaParaPag(i + 1) - 1).getColunas() == 2) {   //Se o lugar que está sendo adicionado é de duas colunas
                                            if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) {   //Se o lugar que está sendo removido é de uma coluna
                                                brancos.get(numQuest).setIcon(aumentaImagem(iconeBranco, iconeSconteudo.getIconHeight(),245));    //Aumenta o tamanho horizontal do fundo branco
                                                if(brancos.get(numQuest).getImp().getAlinhamento().equals("CENTRO")){   //Ajusta alinhamento ao centro
                                                    brancos.get(numQuest).getImp().setLocation((245 - brancos.get(numQuest).getImp().getIcon().getIconWidth())/2, 0);
                                                }else if(brancos.get(numQuest).getImp().getAlinhamento().equals("DIREITA")){    //Ajusta alinhamento à direita
                                                    brancos.get(numQuest).getImp().setLocation(245 - brancos.get(numQuest).getImp().getIcon().getIconWidth(), 0);
                                                }
                                            }
                                        }
                                        areas.get(i + 1).add(brancos.get(numQuest),0);
                                        areas.get(i).remove(brancos.get(numQuest));
                                    }
                                }else if (comps[comps.length - 1] instanceof Alternativa) {
                                    int numQuest = 0;
                                    boolean exclua = false;
                                    for (int j = 0; j < alter.size(); j++) {
                                        if (alter.get(j).getIdentidade() == ((Alternativa)comps[comps.length - 1]).getIdentidade()) {
                                            numQuest = j;
                                            exclua = true;
                                            break;
                                        }
                                    }
                                    if(exclua){
                                        //----------Altera dimensões do fundo branco da imagem-------------
                                        if (papeis.get(areaParaPag(i + 1) - 1).getColunas() == 1) { //Se o lugar que está sendo adicionado é de uma coluna
                                            if (papeis.get(areaParaPag(i) - 1).getColunas() == 2) {   //Se o lugar que está sendo removido é de duas colunas
                                                alter.get(numQuest).setSize(525, 40);
                                            }
                                        }else if (papeis.get(areaParaPag(i + 1) - 1).getColunas() == 2) {   //Se o lugar que está sendo adicionado é de duas colunas
                                            if (papeis.get(areaParaPag(i) - 1).getColunas() == 1) {   //Se o lugar que está sendo removido é de uma coluna
                                                alter.get(numQuest).setSize(245, 40);
                                            }
                                        }
                                        areas.get(i + 1).add(alter.get(numQuest),0);
                                        areas.get(i).remove(alter.get(numQuest));
                                        
                                        alter.get(numQuest).getApag().setBounds(alter.get(numQuest).getWidth() - iconeApagarPeq.getIconWidth() - 8,3,iconeApagarPeq.getIconWidth(), iconeApagarPeq.getIconHeight());
                                        alter.get(numQuest).getApag().setBounds(alter.get(numQuest).getWidth() - iconeNegrito.getIconWidth() - 23,2,iconeNegrito.getIconWidth(), iconeNegrito.getIconHeight());
                                    }
                                }
                            }
                            papeis.get(areaParaPag(i) - 1).setVisible(false);
                            papeis.get(areaParaPag(i) - 1).setVisible(true);
                            jsp.validate();
                        }
                    }
                }
            }
        }
    }
}
