package Telas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import query.Executar;

public class Alunos {
    JFrame janela = new JFrame();
    ArrayList<JCheckBox> checar = new ArrayList<>();
    
    public void gerar(){
        
        
        janela.setTitle("Alunos");
        janela.setSize(500,700);
        janela.setResizable(false);
        janela.setLayout(null);
        janela.setLocationRelativeTo(null);
        //janela.setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        janela.addWindowListener( new WindowAdapter( ){
            public void windowClosing(WindowEvent w){
                janela.dispose();
            }
        });
        
        iniciarComponentes(janela);
        
        janela.setVisible(true);
        
    }
    
    public void iniciarComponentes(JFrame janela){
        JLabel lnome = new JLabel("Nome do Aluno: ");
        lnome.setBounds(10,10,200,20);
        JTextField nome = new JTextField();
        nome.setBounds(10,30,200,20);
        
        JLabel lturma = new JLabel("Turma: ");
        lturma.setBounds(10,50,200,20);
        JTextField turma = new JTextField();
        turma.setBounds(10,70,200,20);
        
        JLabel lmatricula = new JLabel("Matricula: ");
        lmatricula.setBounds(10,90,200,20);
        JTextField matricula = new JTextField();
        matricula.setBounds(10,110,200,20);
        
        JLabel ldisciplinas = new JLabel("Disciplinas");
        ldisciplinas.setBounds(220,10,100, 20);
        
        JButton salvar = new JButton("Salvar");
        
        Executar executar = new Executar();
        ArrayList<String> materias = new ArrayList<>();
        
        try {
            //materias = executar.exibirDisciplinas();
        } catch (Exception ex) {
            Logger.getLogger(Disciplinas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(int i=0;i<materias.size();i++){
            JCheckBox disc = new JCheckBox(materias.get(i));
            disc.setBounds(220,30+(i*20),270, 20);
            checar.add(disc);           
        }
                
        salvar.setBounds(150,620,200,30);
                
        janela.add(lnome);
        janela.add(nome);
        janela.add(lturma);
        janela.add(turma);
        janela.add(lmatricula);
        janela.add(matricula);
        janela.add(ldisciplinas);
        for(int i=0;i<checar.size();i++){
            janela.add(checar.get(i));
        }
        janela.add(salvar);
        
        salvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                Executar executar = new Executar();
                ArrayList<String> discs = new ArrayList<>();
                for(int i=0;i<checar.size();i++){
                    if(checar.get(i).isSelected()){
                        discs.add(checar.get(i).getText());
                    }
                }
                
                try {
                    //executar.CadastrarAlunos(removeAccents(nome.getText().toUpperCase()), removeAccents(turma.getText().toUpperCase()),removeAccents(matricula.getText().toUpperCase()), discs);
                } catch (Exception ex) {
                    Logger.getLogger(Alunos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
    }
    
    public static String removeAccents(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;
    }
    
}
