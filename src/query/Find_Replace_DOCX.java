package query;

import Inicial.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import com.sun.media.sound.InvalidFormatException;

public class Find_Replace_DOCX {
    
    public static void main(String[] args) throws Exception {
        find_replace_in_DOCX();        
    }

    public static void find_replace_in_DOCX() throws IOException,InvalidFormatException,org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        
        try {
            int cont = 0;
            XWPFDocument doc = new XWPFDocument(OPCPackage.open("E:\\EDEF_QUESTOES\\PROGRAMAÇÃO II.docx"));
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.contains("§")) {
                            cont++;
                            text = text.replace("§", "QUESTÃO");
                            r.setText(text, 0);
                        }                      
                    }
                }
            }
            
            doc.write(new FileOutputStream("E:\\EDEF_QUESTOES\\PROGRAMAÇÃO II2.docx"));
            
        }finally{}

    }
    
}