package query;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class LancarNota {
    
    private final String url = "jdbc:mysql://"+new Ip.Servidor().getIp()+":3306/edef";
    private final String username = new Ip.Servidor().getUsuario();
    private final String password = new Ip.Servidor().getSenha();
    
    public int[] pegarGabarito(String matricula){
        int[] gabaritofinal = new int[45];
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select questoes.correta as alternativa,questoes.iddisciplina from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo inner join questoes on questoes.iddisciplina = aluno_disciplina.iddisciplina where alunos.matricula = ? and disciplinas.codigo = \"CCCG\" UNION ALL select questoes.correta as alternativa,questoes.iddisciplina from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo inner join questoes on questoes.iddisciplina = aluno_disciplina.iddisciplina where alunos.matricula = ? and disciplinas.codigo <> \"CCCG\"");
            ps.setString(1, matricula);
            ps.setString(2, matricula);
            ResultSet gabarito = ps.executeQuery();
            
            int i = 0;
            while(gabarito.next()){                
                String valor = gabarito.getString("alternativa");
                gabaritofinal[i] = Integer.parseInt(valor);
                i++;
            }
                        
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return gabaritofinal;
    }
    
    public String[] pegarDisciplinasAlunos(String matricula){
        String[] gabaritoinicial = new String[10];
        String[] gabaritofinal = null;
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select disciplinas.nome as nome,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ?");
            ps.setString(1, matricula);
            ResultSet gabarito = ps.executeQuery();
            
            int i = 0;
            while(gabarito.next()){                
                String valor = gabarito.getString("nome");
                gabaritoinicial[i] = valor;
                i++;
            }
            
            gabaritofinal = new String[i];
            for(int j=0;j<gabaritofinal.length;j++){
                gabaritofinal[j] = gabaritoinicial[j];
            }
            
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return gabaritofinal;
    }
    
    public String[] pegarCodigoDisciplinasAlunos(String matricula){
        String[] gabaritoinicial = new String[10];
        String[] gabaritofinal = null;
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select disciplinas.codigo as codigo,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ? and disciplinas.codigo = \"CCCG\" UNION ALL select disciplinas.codigo as codigo,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ? and disciplinas.codigo <> \"CCCG\"");
            ps.setString(1, matricula);
            ps.setString(2, matricula);
            ResultSet gabarito = ps.executeQuery();
            
            int i = 0;
            while(gabarito.next()){                
                String valor = gabarito.getString("codigo");
                gabaritoinicial[i] = valor;
                i++;
            }
            
            gabaritofinal = new String[i];
            for(int j=0;j<gabaritofinal.length;j++){
                gabaritofinal[j] = gabaritoinicial[j];
            }
            
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return gabaritofinal;
    }
    
    public int qtdDisciplinasQueAlunoFaz(String disciplina){
        int valor = 0;
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select max(questoes.questao) as qtd from questoes where questoes.iddisciplina = ?");
            ps.setString(1, disciplina);
            ResultSet gabarito = ps.executeQuery();
            
            while(gabarito.next()){                
                valor = gabarito.getInt("qtd");
            }
                        
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return valor;
    }
    
    
}
