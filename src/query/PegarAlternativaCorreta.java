package query;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PegarAlternativaCorreta {
    
    private ArrayList<Integer> alternativas = new ArrayList<>();
    
    public ArrayList<Integer> gerar(String caminhodisciplina) {
               
        try{            
            File arq = new File(caminhodisciplina);
            BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(arq), "ISO-8859-1"));
            String linha = lerArq.readLine();
            
                while(linha != null) {
                        
                            try{
                                alternativas.add(Integer.parseInt(linha));
                            }catch(NumberFormatException e){
                                
                            }
                            
                            linha = lerArq.readLine();
                        
                        
                }
                

        
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return alternativas;
        
    }
    
}
